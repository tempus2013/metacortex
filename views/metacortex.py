# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required

from apps.metacortex.models import MetacortexNews, SyllabusSubject

TEMPLATE_ROOT = 'metacortex/'


@login_required
def index(request):
    news = MetacortexNews.objects.filter(is_active=True)
    kwargs = {'news': news}
    return render_to_response(TEMPLATE_ROOT+'base.html', kwargs, context_instance=RequestContext(request))


@login_required
def confirm(request, selected_year, syllabus_from_id, syllabus_to_id):
    if request.method == 'POST':
        if request.POST.get('action', '') == 'yes':
            return redirect(request.session.pop('confirm_yes', ''))
        elif request.POST.get('action', '') == 'no':
            return redirect(request.session.pop('confirm_no', ''))
    else:
        syllabus_to = get_object_or_404(SyllabusSubject, id=syllabus_to_id)
        syllabus_from = get_object_or_404(SyllabusSubject, id=syllabus_from_id)
        request.session['confirm_yes'] = request.GET.get('yes', '')
        request.session['confirm_no'] = request.GET.get('no', '')
        kwargs = {'selected_year': selected_year, 'syllabus_from': syllabus_from, 'syllabus_to': syllabus_to}
        return render_to_response('metacortex/syllabus_my/confirm_copy.html', kwargs, context_instance=RequestContext(request))


def show_help(request):
    kwargs = {}
    return render_to_response(TEMPLATE_ROOT+'show_help.html', kwargs, context_instance = RequestContext(request))