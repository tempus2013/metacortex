# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _

from django.core.paginator import Paginator, InvalidPage, EmptyPage

from datetime import datetime

from apps.trainman.models import Teacher
from apps.merovingian.models import Subject, Module, SGroup, Course
from apps.metacortex.models import SyllabusModule, SyllabusSubject, SyllabusPractice, ECTS, SyllabusToECTS

from apps.metacortex.forms import SearchForm

TEMPLATE_ROOT = 'metacortex/syllabus_add/'

"""
-------------------------------------------------------------
--- BROWSING
-------------------------------------------------------------
"""


@login_required
def index(request):
    kwargs = {}
    return render_to_response(TEMPLATE_ROOT+'base.html', kwargs, context_instance=RequestContext(request))


@login_required
def select_course(request):
    # Search form
    name = request.session.get('syllabus_add_search_name', '')
    if request.method == 'POST':
        search_form = SearchForm(request.POST)
        if search_form.is_valid():
            name = request.session['syllabus_add_search_name'] = search_form.cleaned_data['name']
    else:
        search_form = SearchForm(initial={'name': name})
    
    # Getting courses matching criteria
    courses = Course.objects.didactic_offer_and_future().filter(name__istartswith=name, start_date__gt=datetime(2011, 12, 31))
    
    # Pagination
    paginator = Paginator(courses, 15)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
        
    try:
        courses = paginator.page(page)
    except (EmptyPage, InvalidPage):
        courses = paginator.page(paginator.num_pages)
    
    kwargs = {'courses': courses, 'search_form': search_form}
    return render_to_response(TEMPLATE_ROOT + 'select_course.html', kwargs, context_instance=RequestContext(request))


@login_required
def select_speciality(request, course_id):
    course = get_object_or_404(Course, id=course_id, is_active=True)
    specialities = SGroup.objects.filter(course=course, is_active=True)
    kwargs = {'course': course, 'specialities': specialities}
    return render_to_response(TEMPLATE_ROOT + 'select_speciality.html', kwargs, context_instance=RequestContext(request))


@login_required
def select_module(request, course_id, speciality_id):
    course = get_object_or_404(Course, id=course_id, is_active=True)
    speciality = get_object_or_404(SGroup, id=speciality_id, is_active=True)
    modules = Module.objects.filter(sgroup=speciality, is_active=True)
    kwargs = {'course': course, 'speciality': speciality, 'modules': modules}
    return render_to_response(TEMPLATE_ROOT + 'select_module.html', kwargs, context_instance=RequestContext(request))


@login_required
def select_subject(request, course_id, speciality_id, module_id):
    course = get_object_or_404(Course, id=course_id, is_active=True)
    speciality = get_object_or_404(SGroup, id=speciality_id, is_active=True)
    module = get_object_or_404(Module, id=module_id, is_active=True)
    subjects = Subject.objects.filter(module=module, is_active=True)
    kwargs = {'course': course, 'speciality': speciality, 'module': module, 'subjects': subjects}
    return render_to_response(TEMPLATE_ROOT + 'select_subject.html', kwargs, context_instance=RequestContext(request))

"""
-------------------------------------------------------------
--- ADDING
-------------------------------------------------------------
"""


@login_required
def add_module_syllabus(request, course_id, speciality_id, module_id):
    course = get_object_or_404(Course, id=course_id)
    speciality = get_object_or_404(SGroup, id=speciality_id)
    module = get_object_or_404(Module, id=module_id)
    
    try:
        teacher = request.user.get_profile().teacher
    except Teacher.DoesNotExist:
        messages.error(request, _('You do not have a teacher profile.'))
        return redirect(reverse('apps.metacortex.views.syllabus_add.select_subject', kwargs={'course_id': course.id, 'speciality_id': speciality.id, 'module_id': module.id}))

    if create_module_syllabus(teacher, module):
        messages.success(request, _(u'Syllabus has been added to your profile.'))
    else:
        messages.error(request, _('Syllabus is already assigned to your profile.'))

    return redirect(reverse('apps.metacortex.views.syllabus_add.select_subject', kwargs={'course_id': course.id, 'speciality_id': speciality.id, 'module_id': module.id}))


@login_required
def add_subject_syllabus(request, course_id, speciality_id, module_id, subject_id):
    course = get_object_or_404(Course, id=course_id)
    speciality = get_object_or_404(SGroup, id=speciality_id)
    module = get_object_or_404(Module, id=module_id)
    subject = get_object_or_404(Subject, id=subject_id)

    try:
        teacher = request.user.get_profile().teacher
    except Teacher.DoesNotExist:
        messages.error(request, _('You do not have a teacher profile.'))
        return redirect(reverse('apps.metacortex.views.syllabus_add.select_subject', kwargs={'course_id': course.id, 'speciality_id': speciality.id, 'module_id': module.id}))

    try:
        if create_subject_syllabus(teacher, subject):
            messages.success(request, _(u'Syllabus has been added to your profile.'))
        else:
            messages.error(request, _('Syllabus is already assigned to your profile.'))
    except ECTSError:
        messages.error(request, _(u'En error occured while adding syllabus'))
    
    return redirect(reverse('apps.metacortex.views.syllabus_add.select_subject', kwargs={'course_id': course.id, 'speciality_id': speciality.id, 'module_id': module.id}))


@login_required
def add_practice(request, course_id, speciality_id, module_id, subject_id):
    course = get_object_or_404(Course, id=course_id)
    speciality = get_object_or_404(SGroup, id=speciality_id)
    module = get_object_or_404(Module, id=module_id)
    subject = get_object_or_404(Subject, id=subject_id)

    try:
        teacher = request.user.get_profile().teacher
    except Teacher.DoesNotExist:
        messages.error(request, _('You do not have a teacher profile.'))
        return redirect(reverse('apps.metacortex.views.syllabus_add.select_subject', kwargs={'course_id': course.id, 'speciality_id': speciality.id, 'module_id': module_id}))

    try:
        if create_practice_syllabus(teacher, subject):
            messages.success(request, _(u'Syllabus has been added to your profile.'))
        else:
            messages.error(request, _('Syllabus is already assigned to your profile.'))
    except ECTSError:
        messages.error(request, _(u'En error occured while adding syllabus'))
    
    return redirect(reverse('apps.metacortex.views.syllabus_add.select_subject', kwargs={'course_id': course.id, 'speciality_id': speciality.id, 'module_id': module.id}))


def add_ects(syllabus):
    if syllabus.display_ects_section():
        ectss = ECTS.objects.filter(is_default=True)  # Pobranie wszystkich domyślnych opisów efektów kształcenia
        for ects in ectss:
            SyllabusToECTS.objects.create(syllabus=syllabus, ects=ects)


"""
-------------------------------------------------------------
--- CREATING
-------------------------------------------------------------
"""


class ECTSError(Exception):
    pass


def create_module_syllabus(teacher, module):
    (syllabus, created) = SyllabusModule.objects.get_or_create(module=module)
    if created:
        syllabus.coordinator = teacher
        syllabus.save()
    else:  # Get
        if syllabus.is_active:  # Already created and active
            return False
        else:  # Already created but inactive
            syllabus.coordinator = teacher  # Inactive syllabus should have None coordinator
            syllabus.is_active = True
            syllabus.save()
    return True


def create_subject_syllabus(teacher, subject):
    (syllabus, created) = SyllabusSubject.objects.get_or_create(subject=subject, teacher=teacher)
    if created:
        try:
            add_ects(syllabus)
        except:
            syllabus.delete()
            raise ECTSError
    else:  # Get
        if syllabus.is_active:  # Alredy created and active
            return False
        else:  # Alredy created but inactive
            syllabus.is_active = True
            syllabus.save()
    return True


def create_practice_syllabus(teacher, subject):
    (syllabus, created) = SyllabusPractice.objects.get_or_create(subject=subject, teacher=teacher)
    if not created:  # Syllabus has been already created
        if syllabus.is_active:  # Alredy created and active
            return False
        else:  # Alredy created but inactive
            syllabus.is_active = True
            syllabus.save()
    return True