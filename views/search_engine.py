# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.db.models import Q
from django.contrib import messages
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.utils.translation import ugettext as _

from apps.metacortex.models import SyllabusModule, SyllabusSubject, SyllabusPractice

from apps.metacortex.forms import SyllabusClassicSearchForm

from apps.metacortex.settings import MODULE_TYPE_GENERAL_ID, SYLLABUS_TYPE_MODULE_ID, SYLLABUS_TYPE_SUBJECT_ID, SYLLABUS_TYPE_PRACTICE_ID, SYLLABUS_TYPE_GENERAL_ID

TEMPLATE_ROOT = 'metacortex/search_engine/'


def syllabus_show(request, syllabus_type, syllabus_id):
    try:
        syllabus_type = int(syllabus_type)
        if syllabus_type == SYLLABUS_TYPE_MODULE_ID:
            syllabus = SyllabusModule.objects.get(id=syllabus_id)
            kwargs = {'syllabus_id': syllabus_id, 'syllabus': syllabus}
            template = TEMPLATE_ROOT + 'syllabus_show_module.html'
        elif syllabus_type == SYLLABUS_TYPE_SUBJECT_ID:
            syllabus = SyllabusSubject.objects.get(id=syllabus_id)
            kwargs = {'syllabus_id': syllabus_id, 'syllabus': syllabus}
            template = TEMPLATE_ROOT + 'syllabus_show_subject.html'
        elif syllabus_type == SYLLABUS_TYPE_PRACTICE_ID:
            syllabus = SyllabusPractice.objects.get(id=syllabus_id)
            kwargs = {'syllabus_id': syllabus_id, 'syllabus': syllabus}
            template = TEMPLATE_ROOT + 'syllabus_show_practice.html'
        
        if not (syllabus.is_published or syllabus.is_active):
            messages.info(request, 'Sylabus, którego szukasz nie został opublikowany.')
        else:
            return render_to_response(template, kwargs, context_instance=RequestContext(request))
    except UnboundLocalError:
        messages.error(request, _(u'Syllabus type have not match any of possible values'))
    except:
        raise
        messages.error(request, _(u'En error occured while displaying syllabus'))

    return render_to_response('column_1.html', {}, context_instance=RequestContext(request))


def get_ikey(key):
    """
    Jeżeli argument da się przekonwertować na liczbę całkowitą wtedy ją zwraca, w przeciwnym razie zwraca -1.
    Potrzebne przy wyszukiwaniu po semestrze. Klucz musi być liczbą całkowitą.
    """
    key = key.strip()
    try:
        return int(key)
    except ValueError:
        return 1000  # Dowolna wartość, która będzie różna od możliwych wartości roku zatwierdzenia planu studiów i semestru
    
"""
-----------------------------------------------------------------
--- CLASSIC SYLLABUS SEARCH
-----------------------------------------------------------------
"""


def syllabus_search_classic(request):
    query = request.session.get('query', '')
    syllabus_type = request.session.get('syllabus_type', SYLLABUS_TYPE_SUBJECT_ID)  # Domyślan wartość typu sylabusa: sylabus przedmiotu
    form = SyllabusClassicSearchForm(request.POST or None, initial={'q': query, 'syllabys_type': syllabus_type})
    
    form_was_sent = False  # Określa czy czy wyświetlać informacje o braku wyników
    if request.method == "POST":
        form_was_sent = True
        if form.is_valid():
            query = form.cleaned_data['q']
            syllabus_type = form.cleaned_data['syllabus_type']
            request.session['query'] = query
            request.session['syllabus_type'] = syllabus_type
            syllabusses_list = classic_search(query, syllabus_type)
    else:
        syllabusses_list = classic_search(query, syllabus_type)

    # Paginacja
    paginator = Paginator(syllabusses_list, 10)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
        
    try:
        syllabusses = paginator.page(page)
    except (EmptyPage, InvalidPage):
        syllabusses = paginator.page(paginator.num_pages)    
        
    kwargs = {'form': form, 'syllabusses': syllabusses, 'form_was_sent': form_was_sent}
    return render_to_response(TEMPLATE_ROOT + 'search_classic.html', kwargs, context_instance=RequestContext(request))


def classic_search(query, syllabus_type):
    q = query.split(',')
    syllabus_type = int(syllabus_type)
    if syllabus_type == SYLLABUS_TYPE_MODULE_ID:  # Sylabus modułu
        return module_classic_search_engine(q)
    elif syllabus_type == SYLLABUS_TYPE_SUBJECT_ID:  # Sylabus przedmiotu
        return subject_classic_search_engine(q)
    elif syllabus_type == SYLLABUS_TYPE_PRACTICE_ID:  # Sylabus praktyk
        return practice_classic_search_engine(q)
    elif syllabus_type == SYLLABUS_TYPE_GENERAL_ID:  # Sylabus zajęć ogólnouniwersyteckich
        return general_classic_search_engine(q)
    return None


def module_classic_search_engine(q):
    syllabusses = SyllabusModule.objects.filter(is_published=True, is_active=True)
    for key in q:
        syllabusses = syllabusses.filter(get_classic_module_query(key))
    return syllabusses


def subject_classic_search_engine(q):
    syllabusses = SyllabusSubject.objects.filter(is_published=True, is_active=True)
    for key in q:
        syllabusses = syllabusses.filter(get_classic_subject_query(key))
    return syllabusses.order_by('subject__name', 'additional_name', 'teacher')


def practice_classic_search_engine(q):
    syllabusses = SyllabusPractice.objects.filter(is_published=True, is_active=True)
    for key in q:
        syllabusses = syllabusses.filter(get_classic_practice_query(key))
    return syllabusses


def general_classic_search_engine(q):
    syllabusses = SyllabusSubject.objects.filter(is_published=True, is_active=True, subject__module__type__pk=MODULE_TYPE_GENERAL_ID)
    for key in q:
        syllabusses = syllabusses.filter(get_classic_general_query(key))
    return syllabusses


def get_classic_module_query(key):
    key = key.strip()
    ikey = get_ikey(key)
    return Q(module__sgroup__course__department__name__istartswith=key) | \
        Q(module__sgroup__course__name__istartswith=key) | \
        Q(module__sgroup__course__name__istartswith=key) | \
        Q(module__sgroup__course__level__name__istartswith=key) | \
        Q(module__name__istartswith=key) | \
        Q(coordinator__user_profile__user__last_name__istartswith=key) | \
        Q(code__istartswith=key) | \
        Q(module__sgroup__course__start_date__year=ikey)


def get_classic_subject_query(key):
    key = key.strip()
    ikey = get_ikey(key)
    return Q(subject__module__sgroup__course__department__name__istartswith=key) | \
        Q(subject__module__sgroup__course__name__istartswith=key) | \
        Q(subject__module__sgroup__course__level__name__istartswith=key) | \
        Q(subject__module__name__istartswith=key) | \
        Q(subject__name__istartswith=key) | \
        Q(subject__semester__exact=ikey) | \
        Q(additional_name__istartswith=key) | \
        Q(teacher__user_profile__user__last_name__istartswith=key) | \
        Q(code__istartswith=key) | \
        Q(subject__module__sgroup__course__start_date__year=ikey)


def get_classic_practice_query(key):
    key = key.strip()
    ikey = get_ikey(key)
    return Q(subject__module__sgroup__course__department__name__istartswith=key) | \
        Q(subject__module__sgroup__course__name__istartswith=key) | \
        Q(subject__module__sgroup__course__level__name__istartswith=key) | \
        Q(subject__module__name__istartswith=key) | \
        Q(subject__name__istartswith=key) | \
        Q(subject__semester__exact=ikey) | \
        Q(teacher__user_profile__user__last_name__istartswith=key) | \
        Q(subject__module__sgroup__course__start_date__year=ikey)


def get_classic_general_query(key):
    key = key.strip()
    ikey = get_ikey(key)
    return Q(subject__module__sgroup__course__department__name__istartswith=key) | \
        Q(subject__module__sgroup__course__name__istartswith=key) | \
        Q(subject__module__sgroup__course__level__name__istartswith=key) | \
        Q(subject__module__name__istartswith=key) | \
        Q(subject__name__istartswith=key) | \
        Q(subject__semester__exact=ikey) | \
        Q(additional_name__istartswith=key) | \
        Q(teacher__user_profile__user__last_name__istartswith=key) | \
        Q(code__istartswith=key) | \
        Q(subject__module__sgroup__course__start_date__year=ikey)