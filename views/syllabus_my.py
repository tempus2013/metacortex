# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, get_object_or_404, redirect

from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.db import transaction

from django.db.models import Q
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.utils.translation import ugettext as _

from apps.trainman.models import Teacher, UserProfile
from apps.metacortex.models import Syllabus, SyllabusModule, SyllabusSubject, SyllabusPractice, AssessmentForm, DidacticMethodMajor, DidacticMethodMinor, ECTS, SyllabusToECTS, SyllabusYear
from apps.merovingian.models import Module

from apps.metacortex.forms import SyllabusSubjectForm, SyllabusModuleForm, SyllabusPracticeForm, SearchForm

from apps.metacortex.models import SYLLABUS_PRACTICE, Subject
from apps.trinity.models import ModuleLearningOutcome

from apps.metacortex.views.syllabus_add import create_module_syllabus, create_subject_syllabus, create_practice_syllabus

from apps.metacortex.html_parsers import HTMLParserWYSIWYG
from apps.whiterabbit.views import get_pdf_response
from apps.syjon.lib.functions import utf2ascii
from apps.metacortex.settings import SUBJECT_TYPE_PRACTICE_ID

TEMPLATE_ROOT = 'metacortex/syllabus_my/'


@login_required
def show(request, selected_year=None):
    """
    Wyświetla wszystkie sylabusy zalogowanego pracownika.
    """
    kwargs = {}
    kwargs['SUBJECT_TYPE_PRACTICE_ID'] = SUBJECT_TYPE_PRACTICE_ID

    years = SyllabusYear.objects.all()
    if not years:
        messages.error(request, _(u"List of syllabus years is empty."))
        return redirect(reverse("apps.metacortex.views.metacortex.index"))

    # Getting five last years
    kwargs['years'] = years[:5]
    
    # Getting selected year
    if selected_year is None:
        selected_year = years[0].date.year
    selected_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    kwargs['selected_year'] = selected_year
    
    try:
        # Pobranie wszystkich sylabusów zalogowanego użytkownika.
        # Flaga sylabusów przedmiotu i praktyk po usunięciu z profilu ustawiana jest na False.
        # Po usunięciu sylabusa modułu z profilu zalogowany użytkownik usuwany jest z listy nauczycieli, dlatego w tym przypadku nie ma potrzeby sprawdzania flagi is_active.
        
        teacher = request.user.get_profile().teacher
        
        module_syllabusses = SyllabusModule.objects.filter(coordinator=teacher, module__sgroup__course__start_date__year=selected_year.date.year)
        subject_syllabusses = SyllabusSubject.objects.filter(teacher=teacher, subject__module__sgroup__course__start_date__year=selected_year.date.year, is_active=True).order_by('subject__semester', 'subject__name')
        practice_syllabusses = SyllabusPractice.objects.filter(teacher=teacher, subject__module__sgroup__course__start_date__year=selected_year.date.year, is_active=True)
        
        syllabuses = {}
        
        for subject_syllabus in subject_syllabusses:
            module = subject_syllabus.subject.module
            if not module in syllabuses:
                syllabuses[module] = {'sm': None, 'ss': []}
            syllabuses[module]['ss'].append(subject_syllabus)
        
        for practice_syllabus in practice_syllabusses:
            module = practice_syllabus.subject.module
            if not module in syllabuses:
                syllabuses[module] = {'sm': None, 'ss': []}
            syllabuses[module]['ss'].append(practice_syllabus)
        
        for module_syllabus in module_syllabusses:
            if module_syllabus.module in syllabuses:
                syllabuses[module_syllabus.module]['sm'] = module_syllabus
            else:
                syllabuses[module_syllabus.module] = {'sm': module_syllabus, 'ss': []}
        
        kwargs['syllabuses'] = sorted(syllabuses.items(), key=lambda t: t[0].name)
    except Teacher.DoesNotExist:
        messages.add_message(request, messages.ERROR, _(u'You do not have a teacher profile.'))
    except UserProfile.DoesNotExist:
        messages.add_message(request, messages.ERROR, _(u'You do not have a user profile.'))
    
    return render_to_response(TEMPLATE_ROOT+'show.html', kwargs, context_instance=RequestContext(request))

"""
-----------------------------------------------------------------
--- PRZETWARZANIE DANYCH Z FORMULARZY
-----------------------------------------------------------------
"""


def parse_html_from_wyswig(text):
    """
    Przetwarza kod z edytora WYSIWYG do HTMLa.
    """
    parser = HTMLParserWYSIWYG()
    return parser.parse(text) 

"""
-----------------------------------------------------------------
--- SYLLABUS MODULE
-----------------------------------------------------------------
"""


@login_required
def syllabus_module_edit(request, selected_year, syllabus_id):
    # Preventing from modyfing read only syllabus
    syllabus_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    if syllabus_year.read_only and not (request.user.is_superuser or request.user.has_perm('metacortex.change_syllabus')):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))

    syllabus = get_object_or_404(SyllabusModule, id=syllabus_id)
    
    if request.method == "POST":
        form = SyllabusModuleForm(request.POST)
        if form.is_valid():
            try:
                syllabus.is_published = form.cleaned_data['is_published']
                syllabus.module_description = parse_html_from_wyswig(form.cleaned_data['module_description'])
                syllabus.additional_information = parse_html_from_wyswig(form.cleaned_data['additional_information'])
                syllabus.lecture_languages = form.cleaned_data['lecture_languages']
                syllabus.unit_source = form.cleaned_data['unit_source']
                syllabus.unit_target = form.cleaned_data['unit_target']
                syllabus.save()
            except:
                messages.error(request, _(u'En error occured while saving syllabus.'))
            else:
                messages.success(request, _(u'Syllabus has been successfully saved.'))
            
            # Przekierowanie do kolejnego widoku
            if request.POST.get('submit') == 'save':
                return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
            else:
                return redirect(reverse('apps.metacortex.views.syllabus_my.syllabus_module_edit', kwargs={'selected_year': selected_year, 'syllabus_id': syllabus.id}))
        else:
            messages.error(request, _(u'En error occured while saving syllabus.'))
    else:
        initial = {}
        initial['is_published'] = syllabus.is_published
        initial['module_description'] = syllabus.module_description
        initial['additional_information'] = syllabus.additional_information
        initial['lecture_languages'] = [ll.id for ll in syllabus.lecture_languages.all()]
        initial['unit_source'] = syllabus.unit_source
        initial['unit_target'] = syllabus.unit_target
        form = SyllabusModuleForm(initial)
    
    kwargs = {'form': form, 'selected_year': selected_year, 'syllabus': syllabus}
    return render_to_response(TEMPLATE_ROOT + 'syllabus_module_edit.html', kwargs, context_instance=RequestContext(request))


@login_required
def syllabus_module_show(request, selected_year, syllabus_id):
    """
    Wyświetla sylabus modułu
    """
    syllabus = get_object_or_404(SyllabusModule, id=syllabus_id)
    kwargs = {'selected_year': selected_year, 'syllabus': syllabus}
    return render_to_response(TEMPLATE_ROOT + 'syllabus_module_show.html', kwargs, context_instance=RequestContext(request))


def syllabus_module_delete(request, selected_year, syllabus_id):
    # Preventing from modyfing read only syllabus
    syllabus_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    if syllabus_year.read_only and not (request.user.is_superuser or request.user.has_perm('metacortex.change_syllabus')):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
    
    syllabus = get_object_or_404(SyllabusModule, id=syllabus_id)
    syllabus.is_active = False
    syllabus.coordinator = None
    syllabus.save()
    messages.success(request, _('Sylabus has been removed from your profile.'))
    return redirect('apps.metacortex.views.syllabus_my.show', selected_year=selected_year)


def syllabus_module_print(request, syllabus_id):
    syllabus = get_object_or_404(SyllabusModule, id=syllabus_id)
    template_name = TEMPLATE_ROOT+'syllabus_module_print.html'
    file_name = utf2ascii(unicode(syllabus.module.name))
    template_context = {'syllabus': syllabus}
    return get_pdf_response(request, template_context, template_name, file_name, 'apps.metacortex.views.syllabus_my.show')

"""
-----------------------------------------------------------------
--- SYLLABUS PRACTICE
-----------------------------------------------------------------
"""


@login_required
def syllabus_practice_edit(request, selected_year, syllabus_id):
    # Preventing from modyfing read only syllabus
    syllabus_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    if syllabus_year.read_only and not (request.user.is_superuser or request.user.has_perm('metacortex.change_syllabus')):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))

    syllabus = get_object_or_404(SyllabusPractice, id=syllabus_id)
    if request.method == "POST":
        form = SyllabusPracticeForm(request.POST)
        
        if form.is_valid():
            try:
                syllabus.is_published = form.cleaned_data['is_published']
                syllabus.type = form.cleaned_data['type']
                syllabus.description = parse_html_from_wyswig(form.cleaned_data['description'])
                syllabus.additional_information = parse_html_from_wyswig(form.cleaned_data['additional_information'])
                syllabus.education_effects = parse_html_from_wyswig(form.cleaned_data['education_effects'])
                syllabus.save()
                
                if syllabus.display_ects_section():
                    save_ects(syllabus, request)
            except:
                messages.error(request, _(u'En error occured while saving syllabus.'))
            else:
                messages.success(request, _(u'Syllabus has been successfully saved.'))
                
            if request.POST.get('submit') == 'save':
                return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
            else:
                return redirect(reverse('apps.metacortex.views.syllabus_my.syllabus_practice_edit', kwargs={'selected_year': selected_year, 'syllabus_id': syllabus_id}))
        else:
            messages.error(request, messages.SUCCESS, _(u'Syllabus has been successfully saved.'))
    else:
        initial = {}
        initial['is_published'] = syllabus.is_published
        initial['description'] = syllabus.description
        initial['additional_information'] = syllabus.additional_information
        initial['type'] = syllabus.type
        initial['education_effects'] = syllabus.education_effects
        form = SyllabusPracticeForm(initial)
    
    kwargs = {'form': form, 'selected_year': selected_year, 'syllabus': syllabus}
    return render_to_response(TEMPLATE_ROOT + 'syllabus_practice_edit.html', kwargs, context_instance=RequestContext(request))


@login_required
def syllabus_practice_show(request, selected_year, syllabus_id):
    """
    Wyświetla sylabus przedmiotu.
    """
    syllabus = get_object_or_404(SyllabusPractice, id=syllabus_id)
    kwargs = {'selected_year': selected_year, 'syllabus': syllabus}
    return render_to_response(TEMPLATE_ROOT+'syllabus_practice_show.html', kwargs, context_instance=RequestContext(request))


def syllabus_practice_delete(request, selected_year, syllabus_id):
    # Preventing from modyfing read only syllabus
    syllabus_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    if syllabus_year.read_only and not (request.user.is_superuser or request.user.has_perm('metacortex.change_syllabus')):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
    
    syllabus = get_object_or_404(SyllabusPractice, id=syllabus_id)
    syllabus.is_active = False
    syllabus.save()
    messages.success(request, _(u'Sylabus has been removed from your profile.'))
    return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))


def syllabus_practice_print(request, syllabus_id):
    syllabus = get_object_or_404(SyllabusPractice, id=syllabus_id)
    template_name = TEMPLATE_ROOT+'syllabus_practice_print.html'
    file_name = utf2ascii(unicode(syllabus.subject.name))
    template_context = {'syllabus': syllabus}
    return get_pdf_response(request, template_context, template_name, file_name, 'apps.metacortex.views.syllabus_my.show')

"""
-----------------------------------------------------------------
--- SYLLABUS SUBJECT
-----------------------------------------------------------------
"""


@login_required
def syllabus_subject_edit(request, selected_year, syllabus_id):
    # Preventing from modifying read only syllabus
    syllabus_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    if syllabus_year.read_only and not (request.user.is_superuser or request.user.has_perm('metacortex.change_syllabus')):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))

    syllabus = get_object_or_404(SyllabusSubject, id=syllabus_id)

    # Pole MultipleChoiceField musi otrzymać jako parametr choice listę par (klucz, wartość).
    module_learning_outcomes = tuple((mlo.id, mlo) for mlo in ModuleLearningOutcome.objects.filter(module=syllabus.subject.module))

    if request.method == "POST":
        form = SyllabusSubjectForm(request.POST, module_learning_outcomes=module_learning_outcomes)
        
        if form.is_valid():
            try:
                syllabus.is_published = form.cleaned_data['is_published']
                syllabus.additional_name = form.cleaned_data['additional_name']
                syllabus.subject_difficulty = form.cleaned_data['subject_difficulty']
                syllabus.initial_requirements = parse_html_from_wyswig(form.cleaned_data['initial_requirements'])
                syllabus.subjects_scope = parse_html_from_wyswig(form.cleaned_data['subjects_scope'])
                syllabus.learning_outcomes_verification = parse_html_from_wyswig(form.cleaned_data['learning_outcomes_verification'])
                syllabus.literature = parse_html_from_wyswig(form.cleaned_data['literature'])
                syllabus.module_learning_outcomes = form.cleaned_data['module_learning_outcomes']
                syllabus.assessment_conditions = parse_html_from_wyswig(form.cleaned_data['assessment_conditions'])
                syllabus.additional_information = parse_html_from_wyswig(form.cleaned_data['additional_information'])
                syllabus.education_effects = parse_html_from_wyswig(form.cleaned_data['education_effects'])
                syllabus.save()
                
                save_assessment_forms(syllabus, request.POST.getlist('assessment_forms'))  # Powiązanie form oceniania z sylabusem przedmiotu
                save_didactic_methods(syllabus, request.POST.getlist('didactic_methods'))
                if syllabus.display_ects_section():
                    save_ects(syllabus, request)
            except:
                messages.error(request, _(u'En error occured while saving syllabus.'))
            else:
                messages.success(request, _(u'Syllabus has been successfully saved.'))
                
            if request.POST.get('submit') == 'save':
                return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
            else:
                return redirect(reverse('apps.metacortex.views.syllabus_my.syllabus_subject_edit', kwargs={'selected_year': selected_year, 'syllabus_id': syllabus_id}))
        else:
            messages.error(request, messages.SUCCESS, _(u'Syllabus has been successfully saved.'))
    else:
        initial = {}
        initial['is_published'] = syllabus.is_published
        initial['additional_name'] = syllabus.additional_name
        initial['subject_difficulty'] = syllabus.subject_difficulty.id if syllabus.subject_difficulty else None
        initial['initial_requirements'] = syllabus.initial_requirements
        initial['subjects_scope'] = syllabus.subjects_scope
        initial['module_learning_outcomes'] = syllabus.module_learning_outcomes.values_list('id', flat=True)
        initial['assessment_conditions'] = syllabus.assessment_conditions
        initial['learning_outcomes_verification'] = syllabus.learning_outcomes_verification
        initial['literature'] = syllabus.literature
        initial['additional_information'] = syllabus.additional_information
        initial['education_effects'] = syllabus.education_effects
        form = SyllabusSubjectForm(module_learning_outcomes=module_learning_outcomes, initial=initial)
    
    assessment_forms = get_assessment_forms(syllabus)  # Pobranie listy form oceniania powiązanych z sylabusem przedmiotu
    didactic_methods = get_didactic_methods(syllabus)  # Pobranie listy metod dydaktycznych.
    
    kwargs = {'form': form, 'selected_year': selected_year, 'syllabus': syllabus, 'assessment_forms': assessment_forms, 'didactic_methods': didactic_methods}
    return render_to_response(TEMPLATE_ROOT + 'syllabus_subject_edit.html', kwargs, context_instance=RequestContext(request))


@login_required
def syllabus_subject_show(request, selected_year, syllabus_id):
    """
    Wyświetla sylabus przedmiotu.
    """
    syllabus = get_object_or_404(SyllabusSubject, id=syllabus_id)
    kwargs = {'selected_year': selected_year, 'syllabus': syllabus}
    return render_to_response(TEMPLATE_ROOT+'syllabus_subject_show.html', kwargs, context_instance=RequestContext(request))


def syllabus_subject_delete(request, selected_year, syllabus_id):
    # Preventing from modyfing read only syllabus
    syllabus_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    if syllabus_year.read_only and not (request.user.is_superuser or request.user.has_perm('metacortex.change_syllabus')):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
    
    syllabus = get_object_or_404(SyllabusSubject, id=syllabus_id)
    syllabus.is_active = False
    syllabus.save()
    messages.success(request, _(u'Sylabus has been removed from your profile.'))
    return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))


def syllabus_subject_print(request, syllabus_id):
    syllabus = get_object_or_404(SyllabusSubject, id=syllabus_id)
    template_name = TEMPLATE_ROOT+'syllabus_subject_print.html'
    file_name = utf2ascii(unicode(syllabus.subject.name))
    template_context = {'syllabus': syllabus}
    return get_pdf_response(request, template_context, template_name, file_name, 'apps.metacortex.views.syllabus_my.show')

"""
-----------------------------------------------------------------
--- ECTS
-----------------------------------------------------------------
"""


def save_ects(syllabus, request):
    """
    @param syllabus: Obiekt edytowanego sylabusa.  
    """
    if syllabus.get_ects():
        try:
            # Zapisanie nowego pola z ekwiwalentem punktów ECTS
            custom_ects_name = request.POST['custom_ects_name'].strip()
            if len(custom_ects_name) != 0:  # Użytkownik zdefiniował własny ekwiwalnet punktów ECTS
                custom_ects_value = request.POST['custom_ects_value']
                custom_ects_value = custom_ects_value.replace(',', '.')
                ects = ECTS.objects.create(name=custom_ects_name, order=0)
                SyllabusToECTS.objects.create(syllabus=syllabus, ects=ects, hours=custom_ects_value)
            
            # Zapisanie istniejących pól z ekwiwalentem punktów ECTS
            ects_ids = request.POST.getlist('ects')
            for ects_id in ects_ids:
                hours = request.POST['ects_'+str(ects_id)]  # Pobranie wkwiwalentu godzinowego z pola tekstowego
                hours = hours.replace(',', '.')  # Zamiana separatora ',' na '.'
                if hours is None or hours == '':
                    hours = 0
                    
                ects = ECTS.objects.get(id = ects_id)
                syllabus_to_ects = SyllabusToECTS.objects.get(syllabus=syllabus, ects=ects)
                syllabus_to_ects.hours = hours
                syllabus_to_ects.save()
        except ValueError:
            messages.error(request, _(u'En error occured while saving ECTS section'))
        except KeyError:
            messages.error(request, _(u'En error occured while saving ECTS section'))
        except:
            messages.error(request, _(u'En error occured while saving ECTS section'))


@transaction.commit_on_success
def syllabus_delete_ects(request, selected_year, syllabus_type, syllabus_id, ects_id):
    # Preventing from modifying read only syllabus
    syllabus_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    if syllabus_year.read_only and not (request.user.is_superuser or request.user.has_perm('metacortex.change_syllabus')):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
    
    try:
        ects = ECTS.objects.get(id=ects_id)
        ects.delete()
        messages.success(request, _(u'ECTS equivalen has been successfully deleted'))
    except Syllabus.DoesNotExist:
        messages.error(request, _(u'En error occured while deleting ECTS equivalent'))
    except ECTS.DoesNotExist:
        messages.error(request, _(u'En error occured while deleting ECTS equivalent'))
    except SyllabusToECTS.MultipleObjectsReturned:
        messages.error(request, _(u'En error occured while deleting ECTS equivalent'))
    except:
        messages.error(request, _(u'En error occured while deleting ECTS equivalent'))

    print 'syllabus_type'
    print syllabus_type

    if int(syllabus_type) == SYLLABUS_PRACTICE:
        return redirect(reverse('apps.metacortex.views.syllabus_my.syllabus_practice_edit', kwargs={'selected_year': selected_year, 'syllabus_id': syllabus_id}))
    return redirect(reverse('apps.metacortex.views.syllabus_my.syllabus_subject_edit', kwargs={'selected_year': selected_year, 'syllabus_id': syllabus_id}))

"""
-----------------------------------------------------------------
--- SYLLABUS SUBJECT - ASSESSMENT FORMS
-----------------------------------------------------------------
"""  


def save_assessment_forms(syllabus_subject, assessment_forms_ids):
    """
    Przypisuje formy oceniania do sylabusa przedmiotu.
    @param syllabus_subject: Model sylabusa przedmiotu.
    @param assessment_forms_ids: Lista identyfikatorów form oceniania, które zostaną przypisane do sylabusa przedmiotu.  
    """
    syllabus_subject.assessment_forms.clear()
    for assessment_form_id in assessment_forms_ids:
        assessment_form = AssessmentForm.objects.get(id=assessment_form_id)
        syllabus_subject.assessment_forms.add(assessment_form)


def get_assessment_forms(syllabus_subject):
    """
    Tworzy listę form oceniania z informacją o formach oceniania przypisanych do sylabusa przedmiotu.
    @param syllabus_subject: Model sylabusa przedmiotu.
    """
    assessment_forms_syllabus_subject = syllabus_subject.assessment_forms.all()  # Pobranie wszystkich form oceniania przypisanych do sylabusa.
    assessment_forms_all = AssessmentForm.objects.all()
    assessment_forms = []
    for x in assessment_forms_all:
        assessment_form = {}
        assessment_form['id'] = x.id
        assessment_form['name'] = x.name
        assessment_form['checked'] = "checked" if x in assessment_forms_syllabus_subject else ""
        assessment_forms.append(assessment_form)
    return assessment_forms
    
"""
-----------------------------------------------------------------
--- SYLLABUS SUBJECT - DIDACTIC METHODS
-----------------------------------------------------------------
"""    


def save_didactic_methods(syllabus_subject, didactic_methods_ids):
    """
    Przypisuje formy oceniania do sylabusa przedmiotu.
    @param syllabus_subject: Model sylabusa przedmiotu.
    @param didactic_methods_ids: Lista identyfikatorów metod dydaktycznych, które zostaną przypisane do sylabusa przedmiotu.  
    """
    syllabus_subject.didactic_methods.clear()
    for didactic_method_id in didactic_methods_ids:
        didactic_method = DidacticMethodMinor.objects.get(id=didactic_method_id)
        syllabus_subject.didactic_methods.add(didactic_method)


def get_didactic_methods(syllabus_subject):
    """
    Tworzy listę metod dydaktycznych z informacją o metodach dydaktycznych przypisanych do sylabusa przedmiotu.
    @param syllabus_subject: Model sylabusa przedmiotu.
    """
    didactic_methods_syllabus_subject = syllabus_subject.didactic_methods.all()
    didactic_methods_all = DidacticMethodMajor.objects.all()
    didactic_methods = []
    for x in didactic_methods_all: 
        for minor in x.didacticmethodminor_set.all():
            didactic_method = {}
            didactic_method['id'] = minor.id
            didactic_method['course'] = x.name # Nazwa agregatu dla grupy podrzędnych metod dydaktycznych.
            didactic_method['name'] = minor.name # Nazwa podrzędnej metody dydaktycznej.
            didactic_method['checked'] = "checked" if minor in didactic_methods_syllabus_subject else ""  
            didactic_methods.append(didactic_method)
    return didactic_methods

"""
-----------------------------------------------------------------
--- SYLLABUS SUBJECT COPY
-----------------------------------------------------------------
"""


def syllabus_subject_copy_list(request, selected_year, syllabus_id):
    # Preventing from modifying read only syllabus
    syllabus_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    if syllabus_year.read_only and not (request.user.is_superuser or request.user.has_perm('metacortex.change_syllabus')):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
    
    # Formularz wyszukiwania
    name = request.session.get('syllabus_copy_search_name', '')
    if request.method == 'POST':
        search_form = SearchForm(request.POST)
        if search_form.is_valid():
            name = request.session['syllabus_copy_search_name'] = search_form.cleaned_data['name']
    else:
        search_form = SearchForm(initial={'name': name})
    
    # Pobranie sylabusów spełniających kryteria
    syllabus = get_object_or_404(SyllabusSubject, id=syllabus_id)
    syllabusses_list = SyllabusSubject.objects_published.filter(Q(additional_name__istartswith=name) | Q(subject__name__istartswith=name) | Q(teacher__user_profile__user__last_name__istartswith=name))
    
    # Paginacja
    paginator = Paginator(syllabusses_list, 15)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
        
    try:
        syllabusses = paginator.page(page)
    except (EmptyPage, InvalidPage):
        syllabusses = paginator.page(paginator.num_pages)
    
    kwargs = {'selected_year': selected_year, 'syllabus': syllabus, 'syllabusses': syllabusses, 'search_form': search_form}
    return render_to_response(TEMPLATE_ROOT+'syllabus_subject_copy_list.html', kwargs, context_instance=RequestContext(request)) 


def syllabus_subject_copy(request, selected_year, syllabus_to_id, syllabus_from_id):
    # Preventing from modifying read only syllabus
    syllabus_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    if syllabus_year.read_only and not (request.user.is_superuser or request.user.has_perm('metacortex.change_syllabus')):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))

    syllabus_to = get_object_or_404(SyllabusSubject, id=syllabus_to_id)
    syllabus_from = get_object_or_404(SyllabusSubject, id=syllabus_from_id)
    
    try:
        syllabus_to.is_published = syllabus_from.is_published
        syllabus_to.additional_name = syllabus_from.additional_name
        syllabus_to.subject_difficulty = syllabus_from.subject_difficulty
        syllabus_to.initial_requirements = syllabus_from.initial_requirements
        syllabus_to.subjects_scope = syllabus_from.subjects_scope
        syllabus_to.assessment_conditions = syllabus_from.assessment_conditions
        syllabus_to.module_learning_outcomes = syllabus_from.module_learning_outcomes.all()
        syllabus_to.literature = syllabus_from.literature
        syllabus_to.additional_information = syllabus_from.additional_information
        syllabus_to.education_effects = syllabus_from.education_effects
        syllabus_to.learning_outcomes_verification = syllabus_from.learning_outcomes_verification
        syllabus_to.save()
        
        syllabus_to.assessment_forms.clear()
        for assessment_form in syllabus_from.assessment_forms.all():
            syllabus_to.assessment_forms.add(assessment_form)
            
        syllabus_to.didactic_methods.clear()
        for didactic_method in syllabus_from.didactic_methods.all():
            syllabus_to.didactic_methods.add(didactic_method)

        syllabus_to.ectss.clear()
        for syllabus_to_ects in SyllabusToECTS.objects.filter(syllabus=syllabus_from):
            syllabus_to_ects.id = None
            syllabus_to_ects.syllabus = syllabus_to
            syllabus_to_ects.save()
    except:
        messages.error(request, _(u'En error occured while copying syllabus'))
    else:
        messages.success(request, _(u'Syllabus has been successfully copied'))
    
    return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))


def copy_module_syllabus_data(syllabus_from, syllabus_to):
    syllabus_to.is_published = syllabus_from.is_published
    syllabus_to.module_description = syllabus_from.module_description
    syllabus_to.lecture_languages = syllabus_from.lecture_languages.all()
    syllabus_to.unit_source = syllabus_from.unit_source
    syllabus_to.unit_target = syllabus_from.unit_target
    syllabus_to.coordinator = syllabus_from.coordinator
    syllabus_to.additional_information = syllabus_from.additional_information
    syllabus_to.save()


def copy_subject_syllabus_data(syllabus_from, syllabus_to):
    syllabus_to.is_published = syllabus_from.is_published
    syllabus_to.additional_name = syllabus_from.additional_name
    syllabus_to.subject_difficulty = syllabus_from.subject_difficulty
    syllabus_to.initial_requirements = syllabus_from.initial_requirements
    syllabus_to.subjects_scope = syllabus_from.subjects_scope
    syllabus_to.assessment_conditions = syllabus_from.assessment_conditions
    syllabus_to.module_learning_outcomes = syllabus_from.module_learning_outcomes.all()
    syllabus_to.literature = syllabus_from.literature
    syllabus_to.additional_information = syllabus_from.additional_information
    syllabus_to.education_effects = syllabus_from.education_effects
    syllabus_to.save()
    
    syllabus_to.assessment_forms.clear()
    syllabus_to.assessment_forms = syllabus_from.assessment_forms.all()    
    syllabus_to.didactic_methods.clear()
    syllabus_to.didactic_methods = syllabus_from.didactic_methods.all()
    
    copy_ects_equivalents_data(syllabus_from, syllabus_to)


def copy_practice_syllabus_data(syllabus_from, syllabus_to):
    syllabus_to.is_published = syllabus_from.is_published
    syllabus_to.teacher = syllabus_from.teacher
    syllabus_to.type = syllabus_from.type
    syllabus_to.description = syllabus_from.description
    syllabus_to.education_effects = syllabus_from.education_effects
    syllabus_to.additional_information = syllabus_from.additional_information
    syllabus_to.save()
    
    copy_ects_equivalents_data(syllabus_from, syllabus_to)


def copy_ects_equivalents_data(syllabus_from, syllabus_to):
    syllabus_to.ectss.clear()
    for equivalent in syllabus_from.get_equivalents():
        syllabus_to_ects = SyllabusToECTS()
        syllabus_to_ects.ects = equivalent.ects
        syllabus_to_ects.syllabus = syllabus_to
        syllabus_to_ects.hours = equivalent.hours
        syllabus_to_ects.save()

"""
-----------------------------------------------------------------
--- COPYING FROM LAST YEAR
-----------------------------------------------------------------
"""


def copy_from_last_year(request, selected_year):
    try:
        teacher = request.user.get_profile().teacher
    except Teacher.DoesNotExist:
        messages.error(request, _('You do not have a teacher profile.'))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
    
    # Getting dates
    selected_year = get_object_or_404(SyllabusYear, date__year=selected_year)
    last_year = selected_year.get_previous()
    
    if last_year is None:
        messages.error(request, _('An error occured during processing date'))
        return redirect(reverse('apps.metacortex.views.syllabus_my.show', kwargs={'selected_year': selected_year}))
    
    # Getting syllabuses from last year
    teacher = request.user.get_profile().teacher
    module_syllabuses = SyllabusModule.objects.filter(coordinator=teacher, module__sgroup__course__start_date__year=last_year.date.year, is_active=True)
    subject_syllabuses = SyllabusSubject.objects.filter(teacher=teacher, subject__module__sgroup__course__start_date__year=last_year.date.year, is_active=True).order_by('subject__semester', 'subject__name')
    practice_syllabuses = SyllabusPractice.objects.filter(teacher=teacher, subject__module__sgroup__course__start_date__year=last_year.date.year, is_active=True)
    
    # Creating list of valid modules, subject and practices
    modules = []
    for module_syllabus in module_syllabuses:
        module = module_syllabus.module.get_next()
        if module:
            try:  # Does exist
                SyllabusModule.objects.get(coordinator=teacher, module=module, is_active=True)
            except:  # Doesn't exist
                modules.append(module)
            
    subjects = []
    for subject_syllabus in subject_syllabuses:
        subject = subject_syllabus.subject.get_next()
        if subject:
            try:
                SyllabusSubject.objects.get(teacher=teacher, subject=subject, is_active=True)
            except:
                subjects.append(subject)
            
    practices = []
    for practice_syllabus in practice_syllabuses:
        practice = practice_syllabus.subject.get_next()
        if practice:
            try:
                SyllabusPractice.objects.get(teacher=teacher, subject=practice, is_active=True)
            except:
                practices.append(practice)
    
    context = {}
    context['selected_year'] = selected_year
    context['last_year'] = last_year
    context['modules'] = modules
    context['subjects'] = subjects
    context['practices'] = practices
    return render_to_response(TEMPLATE_ROOT + 'copy_from_last_year.html', context, context_instance=RequestContext(request))


def copy_module_syllabus(request, selected_year, module_id):
    try:
        teacher = request.user.get_profile().teacher
    except Teacher.DoesNotExist:
        messages.error(request, _('You do not have a teacher profile.'))
        return redirect(reverse('apps.metacortex.views.syllabus_my.copy_from_last_year', kwargs={'selected_year': selected_year}))
    
    # Getting modules
    module_to = get_object_or_404(Module, id=module_id)
    module_from = module_to.get_prev()
    
    # Creating syllabus
    create_module_syllabus(teacher, module_to)
    
    # Getting syllabuses
    module_syllabus_from = SyllabusModule.objects.get(module=module_from)
    module_syllabus_to = SyllabusModule.objects.get(module=module_to)
    
    # Copying syllabus
    copy_module_syllabus_data(module_syllabus_from, module_syllabus_to)
    
    messages.success(request, _(u"Module syllabus has been copied"))
    return redirect(reverse('apps.metacortex.views.syllabus_my.copy_from_last_year', kwargs={'selected_year': selected_year}))


def copy_subject_syllabus(request, selected_year, subject_id):
    try:
        teacher = request.user.get_profile().teacher
    except Teacher.DoesNotExist:
        messages.error(request, _('You do not have a teacher profile.'))
        return redirect(reverse('apps.metacortex.views.syllabus_my.copy_from_last_year', kwargs={'selected_year': selected_year}))
    
    # Getting subjects
    subject_to = get_object_or_404(Subject, id=subject_id)
    subject_from = subject_to.get_prev()
    
    # Creating syllabus
    create_subject_syllabus(teacher, subject_to)
    
    # Getting syllabuses
    syllabus_from = SyllabusSubject.objects.get(subject=subject_from, teacher=teacher)
    syllabus_to = SyllabusSubject.objects.get(subject=subject_to, teacher=teacher)
    
    # Copying syllabuses
    copy_subject_syllabus_data(syllabus_from, syllabus_to)
    
    messages.success(request, _(u"Subject syllabus has been copied"))
    return redirect(reverse('apps.metacortex.views.syllabus_my.copy_from_last_year', kwargs={'selected_year': selected_year})) 


def copy_practice_syllabus(request, selected_year, subject_id):
    try:
        teacher = request.user.get_profile().teacher
    except Teacher.DoesNotExist:
        messages.error(request, _('You do not have a teacher profile.'))
        return redirect(reverse('apps.metacortex.views.syllabus_my.copy_from_last_year', kwargs={'selected_year': selected_year}))
    
    # Getting practices
    subject_to = get_object_or_404(Subject, id=subject_id)
    subject_from = subject_to.get_prev()
    
    # Creating syllabus
    create_practice_syllabus(teacher, subject_to)
    
    # Getting syllabuses
    syllabus_from = SyllabusPractice.objects.get(subject=subject_from, teacher=teacher)
    syllabus_to = SyllabusPractice.objects.get(subject=subject_to, teacher=teacher)
    
    # Copying syllabus
    copy_practice_syllabus_data(syllabus_from, syllabus_to)
    
    messages.success(request, _(u"Practice syllabus has been copied"))
    return redirect(reverse('apps.metacortex.views.syllabus_my.copy_from_last_year', kwargs={'selected_year': selected_year})) 