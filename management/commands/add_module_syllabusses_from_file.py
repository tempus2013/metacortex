# -*- coding: utf-8 -*-
'''
Created on 27-08-2012

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.db import transaction

from apps.merovingian.models import Module
from apps.trainman.models import Department, Teacher
from apps.metacortex.models import LectureLanguage, SyllabusModule, ECTS, SyllabusToECTS

import csv

from django.utils import termcolors
green = termcolors.make_style(fg='green')
yellow = termcolors.make_style(fg='yellow')
cyan = termcolors.make_style(fg='cyan')
red = termcolors.make_style(fg='red', opts=('bold',))
bold = termcolors.make_style(opts=('bold',))

class Command(BaseCommand):
    help = u'Dodaje sylabusy modułu z pliku.'

    @transaction.commit_on_success
    def handle(self, *args, **options):
        ifile = open('data/mat/moduly.csv', "rb")
        reader = csv.reader(ifile)
        reader.next() # Pominięcie nagłówka
        
        iteration = 0
        for row in reader:
            # Znalezienie modułu
            try:
                module = Module.objects.get(id = row[1])
                self.stdout.write(green(u"Znaleziono moduł %s: " % unicode(row[1])))
                self.stdout.write("%s\n" % unicode(module))
            except Module.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono modułu %s\n" % unicode(row[1])))

            # Znalezienie jednoski organizacyjnej dla której oferowany jest moduł
            try:
                department = Department.objects.get(id = row[2])
                self.stdout.write(green(u"Znaleziono jednostkę organizacyjną %s: " % unicode(row[2])))
                self.stdout.write("%s\n" % unicode(department))
            except Department.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono jednostki organizacyjnej %s\n" % unicode(row[2])))
            
            # Pobranie opisu modułu
            module_description = row[3]
            self.stdout.write(yellow("Opis modułu: "))
            self.stdout.write("%s\n" % module_description)
                        
            # Pobranie dodatkowych informacji
            additional_information = row[4]
            self.stdout.write(yellow("Dodatkowe informacje: "))
            self.stdout.write("%s\n" % additional_information)
            
            # Znalezienie języków wykładowych
            try:
                lecture_language_ids = row[5].split('/')
                lecture_languages = []
                for lecture_language_id in lecture_language_ids:
                    lecture_language = LectureLanguage.objects.get(id = lecture_language_id)
                    lecture_languages.append(lecture_language)
                    self.stdout.write(green(u"Znaleziono język wykładowy %s: " % unicode(lecture_language_id)))
                    self.stdout.write("%s\n" % unicode(lecture_language))
            except LectureLanguage.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono języka wykładowego %s\n" % unicode(row[5])))
            
            # Znalezienie koordynatora
            try:
                coordinator = Teacher.objects.get(id = row[6])
                self.stdout.write(green(u"Znaleziono koordynatora %s: " % unicode(row[6])))
                self.stdout.write("%s\n" % unicode(coordinator))
            except Teacher.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono koordynatora %s\n" % unicode(row[6])))
            
            # Ekwiwalnety godzinowe punktów ECTS
            try:
                E1 = ECTS.objects.get(id = 5)
                E1H = float(str(row[7]).replace(",", "."))
                self.stdout.write(yellow("G1: "))
                self.stdout.write("%s: %s\n" % (E1H, E1.name))
                
                E2 = ECTS.objects.get(id = 4)
                E2H = float(str(row[8]).replace(",", "."))
                self.stdout.write(yellow("G2: "))
                self.stdout.write("%s: %s\n" % (E2H, E2.name))
                
                E3 = ECTS.objects.get(id = 6)
                E3H = float(str(row[9]).replace(",", "."))
                self.stdout.write(yellow("G3: "))
                self.stdout.write("%s: %s\n" % (E3H, E3.name))
                
                E4 = ECTS.objects.get(id = 8)
                E4H = float(str(row[10]).replace(",", "."))
                self.stdout.write(yellow("G4: "))
                self.stdout.write("%s: %s\n" % (E4H, E4.name))
                
                E5 = ECTS.objects.get(id = 7)
                E5H = float(str(row[11]).replace(",", ".")) 
                self.stdout.write(yellow("G5: "))
                self.stdout.write("%s: %s\n" % (E5H, E5.name))
                
            except ECTS.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono ekwiwalentu godziowego punktów ECTS\n"))
            
            # Dodanie sylabusa
            (syllabus_module, created) = SyllabusModule.objects.get_or_create(module = module)
            
            if created:
                self.stdout.write(cyan(u"Sylabus modułu został utworzony\n"))
            else:
                self.stdout.write(cyan(u"Sylabus modułu został nadpisany\n"))
            
            syllabus_module.is_published = True
            syllabus_module.is_active = True
            syllabus_module.module_description = module_description
            syllabus_module.additional_information = additional_information
            syllabus_module.unit_source = department
            syllabus_module.unit_target = department
            syllabus_module.coordinator = coordinator
            syllabus_module.teachers.add(coordinator)
            
            # Dodanie języków wykładowych
            for lecture_language in lecture_languages:
                syllabus_module.lecture_languages.add(lecture_language)
            syllabus_module.save()
            
            # Dodanie ekwiwalentów godzinowych ECTS
            self.remove_ects(syllabus_module)
            if syllabus_module.display_ects_section():
                SyllabusToECTS.objects.create(syllabus = syllabus_module, ects = E1, hours = E1H)
                SyllabusToECTS.objects.create(syllabus = syllabus_module, ects = E2, hours = E2H)
                SyllabusToECTS.objects.create(syllabus = syllabus_module, ects = E3, hours = E3H)
                SyllabusToECTS.objects.create(syllabus = syllabus_module, ects = E4, hours = E4H)
                SyllabusToECTS.objects.create(syllabus = syllabus_module, ects = E5, hours = E5H)
                self.stdout.write(green(u"Nowe ekwiwalenty godzniowe punktów ECTS zostały dodane\n"))
            else:
                self.stdout.write(red(u"Sylabus nie ma zdefiniowanej sekcji ECTS"))
            
            iteration += 1
            self.stdout.write(bold("\n---------------------: %s\n\n" % iteration))
        
    def remove_ects(self, syllabus):
        syllabusses_to_ects = SyllabusToECTS.objects.filter(syllabus = syllabus)
        if syllabusses_to_ects:
            for syllabus_to_ects in syllabusses_to_ects:
                syllabus_to_ects.delete()
                self.stdout.write('Usuwam pozycję pozycji: %s\n' % syllabus_to_ects.ects)
        else:
            self.stdout.write(cyan('Nie znaleziono pozycji ECTS dla sylabusa: %s\n' % syllabus))