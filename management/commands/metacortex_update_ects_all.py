# -*- coding: utf-8 -*-
'''
Created on 27-08-2012

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.utils import translation
import syjon
from django.conf import settings

from apps.merovingian.models import Course

from apps.metacortex.management.commands.metacortex_update_ects import Command as UpdateECTS

import sys

class Command(BaseCommand):
    help = u'Przegląda moduły i przedmioty wszystkich kierunków z aktywnej ofety dydaktycznej w celu zaktualizowania sekcji ECTS sylabusów.'

    def handle(self, *args, **options):
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))
        
        courses = Course.objects.didactic_offer()
        update_ects = UpdateECTS()      
        update_ects.silent = 1
        
        # Przetworzenie kierunku
        progress = 0.0;
        progress_all = courses.count()
        for course in courses:
            sys.stdout.write('\n%.2f%% (%d/%d) id: %s' % ((progress/progress_all)*100, progress, progress_all, course.id))
            update_ects.process_course(course)
            progress += 1

                