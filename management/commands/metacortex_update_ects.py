# -*- coding: utf-8 -*-
'''
Created on 27-08-2012

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.db import transaction
from django.utils import translation
import syjon
from django.conf import settings

from apps.merovingian.models import Course
from apps.metacortex.models import SyllabusModule, SyllabusSubject, SyllabusPractice, ECTS, SyllabusToECTS

from django.utils import termcolors
green = termcolors.make_style(fg='green')
yellow = termcolors.make_style(fg='yellow')
cyan = termcolors.make_style(fg='cyan')
red = termcolors.make_style(fg='red', opts=('bold',))
bold = termcolors.make_style(opts=('bold',))

import sys

class Command(BaseCommand):
    args = u'<id_course silent>'
    help = u'Przegląda moduły i przedmioty kierunku w celu zaktualizowania sekcji ECTS sylabusów.'

    silent = 0
    
    @transaction.commit_on_success
    def handle(self, *args, **options):
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))
        
        id_course = args[0] 
        
        # Pobranie kierunku
        try:
            course = Course.objects.get(id = id_course)
        except Course.DoesNotExist:
            raise CommandError('Nie znaleziono kierunk\n')
        
        self.process_course(course)
        
    def process_course(self, course):
        self.out(bold('Kierunek: %s\n' % unicode(course).encode('utf-8')))

        # Sylabusy przedmiotu
        self.out(bold('Sylabusy przedmiotów\n'))
        subject_syllabusses = SyllabusSubject.objects.filter(subject__module__sgroup__course = course)
        self.process_syllabusses(subject_syllabusses)
        
        # Sylabus praktyk
        self.out(bold('Sylabusy praktyk\n'))
        practice_syllabusses = SyllabusPractice.objects.filter(subject__module__sgroup__course = course)
        self.process_syllabusses(practice_syllabusses)
    
    def process_syllabusses(self, syllabusses):
        if syllabusses:
            for syllabus in syllabusses:
                self.out(bold('Przetwarzanie sylabusa: %s (%s)\n' % (syllabus, syllabus.id)))
                if syllabus.display_ects_section():
                    self.out(bold('\tDodaję sekcję ects do sylabusa\n'))
                    self.add_ects(syllabus)
                else: # Sylabus nie powinien mieć sekcji ECTS
                    self.out(bold('\tUsuwam sekcję ects z sylabusa\n'))
                    self.remove_ects(syllabus)
        else:
            self.out(yellow('\tNie znaleziono żadnego sylabusa\n'))
        self.out('\n')
        
    def add_ects(self, syllabus):
        ectss = ECTS.objects.filter(is_default = True) # Pobranie wszystkich domyślnych opisów efektów kształcenia
        for ects in ectss:
            try:
                SyllabusToECTS.objects.get(syllabus = syllabus, ects = ects)
                self.out(cyan('\tSylabus ma już przypisaną pozycję: %s\n' % ects))
            except SyllabusToECTS.DoesNotExist:
                SyllabusToECTS.objects.create(syllabus = syllabus, ects = ects)
                self.out(green('\tDodano pozycję: %s\n' % ects))
                if self.silent:
                    self.out(bold(green('+')), 1)
            except SyllabusToECTS.MultipleObjectsReturned:
                ects_equivalents = iter(SyllabusToECTS.objects.filter(syllabus = syllabus, ects = ects))
                ects_equivalents.next()
                for ects_equivalent in ects_equivalents:
                    ects_equivalent.delete()
                    self.out(red('\Usunięto pozycję: %s\n' % ects))
                    if self.silent:
                        self.out(bold(red('-')), 1)
        self.out('\n')
    
    def remove_ects(self, syllabus):
        syllabusses_to_ects = SyllabusToECTS.objects.filter(syllabus = syllabus)
        if syllabusses_to_ects:
            for syllabus_to_ects in syllabusses_to_ects:
                syllabus_to_ects.delete()
                self.out('\tUsuwam pozycję pozycji: %s\n' % syllabus_to_ects.ects)
        else:
            self.out(cyan('\tNie znaleziono pozycji ECTS dla sylabusa: %s\n' % syllabus))

    def out(self, message, force_print=0):
        if not self.silent or force_print:
            sys.stdout.write(message)