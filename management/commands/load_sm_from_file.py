# -*- coding: utf-8 -*-
'''
Created on 27-08-2012

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import termcolors
import syjon
from django.conf import settings
from django.utils import translation
from django.db import transaction
from xml.dom import minidom
import sys

from apps.metacortex.models import LectureLanguage, SyllabusModule
from apps.merovingian.models import Module
from apps.trainman.models import Department, Teacher

green = termcolors.make_style(fg='green')
yellow = termcolors.make_style(fg='yellow')
cyan = termcolors.make_style(fg='cyan')
red = termcolors.make_style(fg='red', opts=('bold',))
bold = termcolors.make_style(opts=('bold',))

class Command(BaseCommand):
    args = u'<path>'
    help = u'path - Path to a XML file with Module Learning Outcomes'

    @transaction.commit_on_success
    def handle(self, *args, **options):
        """
        Loads module learning outcomes from XML file. Sample XML:
        path = '../syjon/data/trinity_xml/.xml'
        <module id="12">
            <syllabus>
                <is_published>1</is_published>
                <unit_target id="5"></unit_target>
                <unit_source id="66"></unit_source>
                <module_description>xxx</module_description>
                <additional_remarks>zzz</additional_remarks>
                <lecture_languages>
                    <lecture_language id="1"></lecture_language>
                    <lecture_language id="2"></lecture_language>
                </lecture_languages>
                <coordinator id="23456"></coordinator>
                <ects_contact_teacher value="30"></ects_contact_teacher>
                <ects_classes value="30"></ects_classes value>
                <ects_prepare_for_classes value="30"></ects_prepare_for_classes>
                <ects_prepare_for_exam value="30"></ects_prepare_for_exam>
                <ects_self_study value="30"></ects_self_study>
            </syllabus>
        </module>
        """
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))
            
        try:    
            path = args[0]
        except:
            print red("Insufficient number of arguments")
            return
        
        xml_dom = minidom.parse(path)
        
        for xml_module in xml_dom.getElementsByTagName('module'):
            # Getting module object
            module_id = xml_module.getAttribute('id')
            try:
                module = Module.objects.get(id = module_id)
            except Module.DoesNotExist:
                self.stdout.write(red("\n\nModul nie istnieje, id: %s\n\n") % unicode(module_id))
            self.stdout.write(green("Module: %s\n" % unicode(module).encode('utf-8')))
            
            # Creating new syllabus module object
            try:
                sm = SyllabusModule.objects.get(module = module)
                sm.delete()
            except SyllabusModule.DoesNotExist:
                pass
            sm = SyllabusModule()
            sm.module = module            
            
            # Getting fields from XML file
            is_published = True if xml_module.getElementsByTagName('is_published')[0].firstChild.nodeValue == '1' else False 
            module_description = xml_module.getElementsByTagName('module_description')[0].firstChild.nodeValue
            xml_module = xml_module.getElementsByTagName('syllabus')[0] # Getting this field is required to get another fields listed below.
            unit_target_id = xml_module.getElementsByTagName('unit_target')[0].getAttribute('id')
            unit_source_id = xml_module.getElementsByTagName('unit_source')[0].getAttribute('id')
            coordinator_id = xml_module.getElementsByTagName('coordinator')[0].getAttribute('id')
            xml_additional_remarks = xml_module.getElementsByTagName('additional_remarks')[0]
            additional_remarks = xml_additional_remarks.firstChild.nodeValue if xml_additional_remarks.firstChild else ""
            
            # Creating objects of fields with foreign key
            ut = Department.objects.get(id = unit_target_id)
            us = Department.objects.get(id = unit_source_id)

            co = Teacher.objects.get(id = coordinator_id)

            # Adding values to new syllabus module object
            sm.is_published = is_published
            sm.module_description = module_description
            sm.unit_target = ut
            sm.unit_source = us
            sm.additional_information = additional_remarks # Uznałem że additional remarks to additional_information.
            sm.coordinator = co

            self.stdout.write(yellow("\tModule description: %s\n" % unicode(sm.module_description).encode('utf-8')))
            self.stdout.write(yellow("\tUnit target: %s\n" % unicode(sm.unit_target).encode('utf-8')))
            self.stdout.write(yellow("\tUnit source: %s\n" % unicode(sm.unit_source).encode('utf-8')))
            self.stdout.write(yellow("\tAdditional remarks: %s\n" % unicode(sm.additional_information).encode('utf-8')))
            self.stdout.write(yellow("\tCoordinator: %s\n" % unicode(sm.coordinator).encode('utf-8')))

            # Saving new syllabus module object
            sm.save()
            
            # Adding lecure languages
            xml_lecture_languages = xml_module.getElementsByTagName('lecture_languages')[0]
            for xml_lecture_language in xml_lecture_languages.getElementsByTagName('lecture_language'):
                lecture_language_id = xml_lecture_language.getAttribute('id')
                ll = LectureLanguage.objects.get(id = lecture_language_id) # Sądzę że wyczytałem że klasa models.Model ma zawsze auto-pole id.
                sm.lecture_languages.add(ll)
                self.stdout.write(cyan("\t\tLecture language: %s\n" % unicode(ll.name).encode('utf-8')))
                 