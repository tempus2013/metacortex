# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import translation
import syjon
from django.conf import settings

import csv

from django.utils import termcolors
green = termcolors.make_style(fg='green')
yellow = termcolors.make_style(fg='yellow')
cyan = termcolors.make_style(fg='cyan')
red = termcolors.make_style(fg='red', opts=('bold',))
bold = termcolors.make_style(opts=('bold',))

from apps.trainman.models import Teacher
from apps.merovingian.models import Course, Subject

"""
1. Stworzenie struktury danych z modułami
1.1 Dopasowanie nauczycieli
1.2 Dopasowanie kierunków
2. Stworzenie struktury danych z przedmiotami
2.1 Dopasowanie nauczycieli
2.2 Dopasowanie przedmiotów
3. Policzenie stattystyk

Po odnalezieniu przedmiotów dokonuje się automatycznie dopasowanie modułów do przedmiotów.
"""

MULTIPLE = 0
VALID = 1
INVALID = 2
NOT_PUBLISHED = 3
WRONG_LANGUAGE = 4
NOT_EXISTS = 5
NO_VALID_COURSE = 6
EMPTY_NAME = 7
NO_COURSE_IN_COURSES_LIST = 8

class Command(BaseCommand):
    help = u'Dodaje sylabusy modułu z pliku.'

    @transaction.commit_on_success
    def handle(self, *args, **options):
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))
        
        # DANE DO SYLABUSÓW
        ifile  = open('data/sjk/term_data.csv', "rb")
        reader = csv.reader(ifile, delimiter=';', quotechar='"')
        term_data = {}
        for row in reader:
            term_data[row[0]] = row[2]
        print "Dane do sylabusów wczytane."
                    
        # SYLABUSY MODUŁU
        ifile  = open('data/sjk/sjk_main.csv', "rb")
        reader = csv.reader(ifile, delimiter=';', quotechar='"')
        main = self.load_main(reader)
        print "Sylabusy ogólne wczytane."
        
        # SYLABUSY PRZEDMIOTU
        ifile  = open('data/sjk/sjk_lecture.csv', "rb")
        reader = csv.reader(ifile, delimiter=';', quotechar='"')
        lecture = self.load_lecture(reader, main)
        print "Sylabusy szczegółowe wczytane."

        # STATYSTYKI
        self.stats(lecture, main)
        
    def load_main(self, csv):
        s = {}
        for row in csv:
            if row[0].isdigit():
                d = {}
                d['name'] = row[1].strip()
                d['course'] = self.proc_course(row[2], row[3])
                d['level'] = row[3]
                d['unit_source'] = row[4]
                d['unit_target'] = row[5]
                d['type'] = row[6]
                d['description'] = row[7]
                d['languages'] = row[8]
                d['teacher'] = self.proc_teacher(row[9]) # KOORDYNATOR
                d['additional_information'] = row[10]
                s[row[0].strip()] = d 
        return s
    
    def load_lecture(self, csv, main):
        s = {}
        for row in csv:
            d = {}
            d['published'] = row[2]
            d['teacher'] = self.proc_teacher(row[6]) # NAUCZYCIEL
            stype = row[10].strip()
            semester = self.proc_semester(row[4].strip())
            d['subject'] = self.proc_subject(row[1], stype, semester, row[0], main) # PRZEDMIOT
            s[row[0].strip()] = d 
        return s

# ----------------------
# --- SEMESTER
# ---------------------- 

    def proc_semester(self, semester):
        if semester == 'I':
            return 1
        elif semester == 'II':
            return 2
        elif semester == 'III':
            return 3
        elif semester == 'IV':
            return 4
        elif semester == 'V':
            return 5
        elif semester == 'VI':
            return 6
        elif semester == 'VII':
            return 7
        elif semester == 'VIII':
            return 8
        elif semester == 'IX':
            return 9
        elif semester == 'X':
            return 10
# ----------------------
# --- SUBJECT
# ---------------------- 

    def proc_subject(self, name, stype, semester, module_id, module_syllabusses):                 
        result = {}
        result['object'] = "%s, %s, %s" % (name, stype, semester)

        if module_syllabusses.has_key(module_id):
            module_syllabus = module_syllabusses[module_id]
            if module_syllabus['course']['status'] == VALID:
                course = module_syllabus['course']['object']
                subject_name = module_syllabus['name'] 
                try:
                    result['object'] = Subject.objects.get(module__sgroup__course = course, name__iexact = subject_name, semester = semester, type__name__iexact = stype)
                    result['status'] = VALID
                except Subject.DoesNotExist:
                    result['status'] = NOT_EXISTS
                except Subject.MultipleObjectsReturned:
                    result['status'] = MULTIPLE
            else:
                result['status'] = NO_VALID_COURSE
        else:
            result['status'] = NO_COURSE_IN_COURSES_LIST

            
        return result
 
# ----------------------
# --- COURSE
# ----------------------   
    
    def proc_course(self, name, other):    
        s = {}
        
        if other.find('niestacjonarne') != -1 or other.find('niest.') != -1:
            s['type'] = 'niestacjonarny'
        elif other.find('stacjonarne') != -1:
            s['type'] = 'stacjonarny'
        
        if other.find('studia I st.') != -1:
            s['level'] = 'I stopień'
        elif other.find('studia II st.') != -1:
            s['level'] = 'II stopień'
        elif other.find('studia doktoranckie') != -1:
            s['level'] = 'III stopień'
        elif other.find('studia jedn. mag.') != -1:
            s['level'] = 'jednolite magisterskie'
        elif other.find('studia podyplomowe') != -1:
            s['level'] = 'podyplomowe dokształcające'
        
        if len(s) != 2:
            s['other'] = other

        result = {}
        result['object'] = name
        if s.has_key('other'):
            result['status'] = INVALID
        else:
            name = name.strip()
            mtype = s['type'].strip()
            level = s['level'].strip()
            
            try:
                result['object'] = Course.objects.get(name__iexact = name, type__name = mtype, level__name = level, start_date__year = '2011') 
                result['status'] = VALID
            except Course.DoesNotExist:
                result['status'] = NOT_EXISTS
            except Course.MultipleObjectsReturned:
                result['status'] = MULTIPLE           
        
        return result
 
# ----------------------
# --- TEACHER
# ----------------------   
        
    def proc_teacher(self, s):
        teacher = {}
        
        spl = s.split(',')
        if len(spl) == 3 and not (len(spl[0].strip().split(' ')) != 1 or len(spl[1].strip().split(' ')) != 1): # Koordynator został wprowadzony w stadardzie [nazwisko, imię, tytuł] i nazwiso lub imię składają się tylko z jednego wyrazu
            teacher['last_name'] = spl[0].strip()
            teacher['first_name'] = spl[1].strip()
            teacher['title'] = spl[2].strip()
        if len(spl) == 3 and len(spl[0].strip().split(' ')) == 1 and len(spl[1].strip().split(' ')) == 2: # Koordynator został wprowadzony w stadardzie [nazwisko, imię, tytuł], nazwiso składa się z jednego wyrazu i imię składa się z dwóch wyrazów
            teacher['last_name'] = spl[0].strip()
            teacher['first_name'] = spl[1].strip().split(' ')[0].strip()
            teacher['title'] = spl[2].strip()
        elif len(spl) == 2 and len(spl[0].strip().split(' ')) == 2: # Koordynator został wprowadzony w standardzie [nazwisko imię, tytuł]
            teacher['last_name'] = spl[0].strip().split(' ')[0].strip()
            teacher['first_name'] = spl[0].strip().split(' ')[1].strip()
            teacher['title'] = spl[1].strip()
        
        if len(teacher) != 3 or teacher['first_name'].strip() == '' or teacher['last_name'].strip() == '':
            if s.strip() == '':
                s = '-------'
            teacher['full_name'] = s.replace('\n', ' ')
        
        
        result = {}
        result['object'] = s
        if len(teacher) == 3:
            last_name = teacher['last_name']
            first_name = teacher['first_name']
            title = teacher['title']

            try:
                result['object'] = Teacher.objects.get(user_profile__user__last_name = last_name, user_profile__user__first_name = first_name)
                result['status'] = VALID
            except Teacher.DoesNotExist:
                result['status'] = NOT_EXISTS
            except Teacher.MultipleObjectsReturned:
                result['status'] = MULTIPLE
        else:
            result['status'] = INVALID
        
        return result
    
# ----------------------
# --- STATYSTYKI
# ----------------------

    def stats(self, subject_syllabusses, module_syllabusses):
        print bold("\nSYLABUSY OGÓLNE")
        print bold("------------------------------\n")
        
        valid = invalid = not_exists = multiple = no_valid_course = no_course_in_courses_list = empty_name = 0
        for value in module_syllabusses.values():
            if value['teacher']['status'] == VALID:
                valid += 1
            elif value['teacher']['status'] == NOT_EXISTS:
                not_exists += 1
            elif value['teacher']['status'] == MULTIPLE:
                multiple += 1
            elif value['teacher']['status'] == INVALID:
                invalid += 1
        
        n = float(len(module_syllabusses))        
        print "NAUCZYCIELE:"
        print green("%s: %s, %.2f%%" % ("Nauczyciele rozpoznani", valid, (valid/n)*100))
        print "%s: %s, %.2f%%" % ("Nauczyciele nierozpoznani", not_exists, (not_exists/n)*100)
        print "%s: %s, %.2f%%" % ("Nauczyciele rozpoznani niejednoznacznie", multiple, (multiple/n)*100)
        print "%s: %s, %.2f%%" % ("Nauczyciele wprowadzeni z błędem", invalid, (invalid/n)*100)
        print ""
        
        valid = invalid = not_exists = multiple = no_valid_course = no_course_in_courses_list = empty_name = 0
        for value in module_syllabusses.values():
            if value['course']['status'] == VALID:
                valid += 1
            elif value['course']['status'] == INVALID:
                invalid += 1
            elif value['course']['status'] == NOT_EXISTS:
                not_exists += 1
            elif value['course']['status'] == MULTIPLE:
                multiple += 1
                
        n = float(len(module_syllabusses))        
        print "KIERUNKI:"
        print green("%s: %s, %.2f%%" % ("Kierunki rozpoznani", valid, (valid/n)*100))
        print "%s: %s, %.2f%%" % ("Kierunki nierozpoznani", not_exists, (not_exists/n)*100)
        print "%s: %s, %.2f%%" % ("Kierunki rozpoznani niejednoznacznie", multiple, (multiple/n)*100)
        print "%s: %s, %.2f%%" % ("Kierunki wprowadzeni z błędem", invalid, (invalid/n)*100)
        print ""
        
        valid = invalid = not_exists = multiple = no_valid_course = no_course_in_courses_list = empty_name = 0
        for value in subject_syllabusses.values():
            if value['teacher']['status'] == VALID:
                valid += 1
            elif value['teacher']['status'] == NOT_EXISTS:
                not_exists += 1
            elif value['teacher']['status'] == MULTIPLE:
                multiple += 1
            elif value['teacher']['status'] == INVALID:
                invalid += 1
                
        print bold("SYLABUSY SZCZEGÓŁOWE")
        print bold("------------------------------\n")
                
        n = float(len(subject_syllabusses))        
        print "NAUCZYCIELE:"
        print green("%s: %s, %.2f%%" % ("Nauczyciele rozpoznani", valid, (valid/n)*100))
        print "%s: %s, %.2f%%" % ("Nauczyciele nierozpoznani", not_exists, (not_exists/n)*100)
        print "%s: %s, %.2f%%" % ("Nauczyciele rozpoznani niejednoznacznie", multiple, (multiple/n)*100)
        print "%s: %s, %.2f%%" % ("Nauczyciele wprowadzeni z błędem", invalid, (invalid/n)*100)
        print ""
        
        valid = invalid = not_exists = multiple = no_valid_course = no_course_in_courses_list = empty_name = 0        
        for value in subject_syllabusses.values():
            if value['subject']['status'] == VALID:
                valid += 1
            elif value['subject']['status'] == NOT_EXISTS:
                not_exists += 1
            elif value['subject']['status'] == MULTIPLE:
                multiple += 1
            elif value['subject']['status'] == NO_VALID_COURSE:
                no_valid_course += 1
            elif value['subject']['status'] == NO_COURSE_IN_COURSES_LIST:
                no_course_in_courses_list += 1
        
        n = float(len(subject_syllabusses))
        print "PRZEDMIOTY:"
        print green("%s: %s, %.2f%%" % ("Przedmioty rozpoznane", valid, (valid/n)*100))
        print "%s: %s, %.2f%%" % ("Przedmioty nierozpoznane", not_exists, (not_exists/n)*100)
        print "%s: %s, %.2f%%" % ("Przedmioty rozpoznane niejednoznacznie", multiple, (multiple/n)*100)
        print "%s: %s, %.2f%%" % ("Przedmioty z nierozpoznanym kierunkiem", no_valid_course, (no_valid_course/n)*100)
        print "%s: %s, %.2f%%" % ("Przedmioty bez kierunku", no_course_in_courses_list, (no_course_in_courses_list/n)*100)
        print ""
        
        print bold("PODSUMOWANIE")
        print bold("------------------------------\n")
        
        valid = 0
        n = float(len(module_syllabusses))
        for value in module_syllabusses.values():
            if value['course']['status'] == VALID and value['teacher']['status'] == VALID:
                valid += 1
        
        print "%s: %s, %.2f%%" % ("Poprawne sylabusy ogólne", valid, (valid/n)*100)
        
        valid = 0
        n = float(len(subject_syllabusses))        
        for value in subject_syllabusses.values():
            if value['subject']['status'] == VALID and value['teacher']['status'] == VALID:
                valid += 1
                
        print "%s: %s, %.2f%%" % ("Poprawne sylabusy szczegółowe", valid, (valid/n)*100)