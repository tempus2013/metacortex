# -*- coding: utf-8 -*-
'''
Created on 27-08-2012

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation
import syjon
from django.conf import settings

from apps.merovingian.models import Course, Module
from apps.metacortex.models import SyllabusModule, SyllabusSubject, SyllabusPractice
from apps.syjon.lib.functions import utf2ascii
from apps.whiterabbit.views import get_pdf_file

class Command(BaseCommand):
    args = u'<id_course>'
    help = u'Wyświetla informacje o sylabusach wprowadzonych dla danego kierunku.'

    def handle(self, *args, **options):    
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))
        self.create_subject_syllabuses()
        self.create_practice_syllabuses()
        self.create_module_syllabuses()


    def create_subject_syllabuses(self):
        template_name = 'metacortex/syllabus_my/syllabus_subject_print.html'

        for course in Course.objects.all():
            for sgroup in course.sgroup_set.all():
                for module in sgroup.modules.all():
                    for subject in module.subject_set.all():
                        path = u'/tmp/metacortex/ss/%s/%s/%s' % (unicode(course), unicode(sgroup.name), unicode(module.name))
                        syllabusses = SyllabusSubject.objects.filter(subject=subject, is_published=True)
                        for index, syllabus in enumerate(syllabusses):
                            semester = subject.get_year_semester()
                            file_name = u'%02d - %s - %s - semestr %s.pdf' % (index, unicode(syllabus.teacher), unicode(syllabus.subject.name), semester[1])
                            template_context = {'syllabus': syllabus}

                            get_pdf_file(template_context, template_name, file_name, {}, path=path)

                            self.stdout.write('Utworzono sylabus: %s\n' % (file_name))

    def create_practice_syllabuses(self):
        template_name = 'metacortex/syllabus_my/syllabus_practice_print.html'

        for course in Course.objects.all():
            for sgroup in course.sgroup_set.all():
                for module in sgroup.modules.all():
                    for subject in module.subject_set.all():
                        path = u'/tmp/metacortex/sp/%s/%s/%s' % (unicode(course), unicode(sgroup.name), unicode(module.name))
                        syllabusses = SyllabusPractice.objects.filter(subject=subject, is_published=True)
                        for index, syllabus in enumerate(syllabusses):
                            semester = subject.get_year_semester()
                            file_name = u'%02d - %s - %s - semestr %s.pdf' % (index, unicode(syllabus.teacher), unicode(syllabus.subject.name), semester[1])
                            template_context = {'syllabus': syllabus}
                            get_pdf_file(template_context, template_name, file_name, {}, path=path)
                            self.stdout.write('Utworzono sylabus: %s\n' % (file_name))

    def create_module_syllabuses(self):
        template_name = 'metacortex/syllabus_my/syllabus_module_print.html'

        for course in Course.objects.all():
            for sgroup in course.sgroup_set.all():
                for module in sgroup.modules.all():
                    path = u'/tmp/metacortex/sm/%s/%s/%s' % (unicode(course), unicode(sgroup.name), unicode(module.name))
                    syllabusses = SyllabusModule.objects.filter(module=module, is_published=True)
                    for index, syllabus in enumerate(syllabusses):
                        file_name = u'%02d - %s - %s.pdf' % (index, unicode(syllabus.coordinator), unicode(syllabus.module.name))
                        template_context = {'syllabus': syllabus}
                        get_pdf_file(template_context, template_name, file_name, {}, path=path)
                        self.stdout.write('Utworzono sylabus: %s\n' % (file_name))
