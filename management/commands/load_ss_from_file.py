# -*- coding: utf-8 -*-
'''
Created on 27-08-2012

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import termcolors
import syjon
from django.conf import settings
from django.utils import translation
from django.db import transaction
from xml.dom import minidom
import sys

from apps.metacortex.models import AssessmentForm, DidacticMethodMinor, ECTS, SyllabusToECTS, SubjectDifficulty, SyllabusSubject
from apps.merovingian.models import Subject
from apps.trainman.models import Department, Teacher

green = termcolors.make_style(fg='green')
yellow = termcolors.make_style(fg='yellow')
cyan = termcolors.make_style(fg='cyan')
red = termcolors.make_style(fg='red', opts=('bold',))
bold = termcolors.make_style(opts=('bold',))

class Command(BaseCommand):
    args = u'<path>'
    help = u'path - Path to a XML file with Module Learning Outcomes'

    @transaction.commit_on_success
    def handle(self, *args, **options):
        """
        path = '../syjon/data/trinity_xml/.xml'
        <subject id="34">
            <syllabuses>
                <syllabus>
                    <teacher id="222"></teacher>
                    <is_published>1</is_published>
                    <additional_name>Sylabuz pszedmjotowy</additional_name>
                    <subject_difficulty id="1"></subject_difficulty>
                    <assessment_forms>
                        <assessment_form id="44"></assessment_form>
                        <assessment_form id="45"></assessment_form>
                    </assessment_forms>
                    <didactic_methods>
                        <didactic_method id="22"></didactic_method>
                        <didactic_method id="23"></didactic_method>
                    </didactic_methods>
                    <initial_requirements>zzz</initial_requirements>
                    <subjects_scope>xxx</subjects_scope>
                    <literature>yyy</literature>
                    <additional_remarks>iii</additional_remarks>
                    <additional_learning_outcomes>rrr</additional_learning_outcomes>
                    <ects_contact_teacher value="30"></ects_contact_teacher>
                    <ects_classes value="30"></ects_classes value>
                    <ects_prepare_for_classes value="30"></ects_prepare_for_classes>
                    <ects_prepare_for_exam value="30"></ects_prepare_for_exam>
                    <ects_self_study value="30"></ects_self_study>
                </syllabus>
            </syllabuses>
        </subject>
        """
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))
            
        try:    
            path = args[0]
        except:
            print red("Insufficient number of arguments")
            # return
        
        xml_dom = minidom.parse(path)
        
        xml_subjects = xml_dom.getElementsByTagName('subjects')[0]
        for xml_subject in xml_subjects.getElementsByTagName('subject'):
            # Getting subject object
            subject_id = xml_subject.getAttribute('id')
            subject = Subject.objects.get(id = subject_id)

            self.stdout.write(green("Subject: %s\n" % unicode(subject).encode('utf-8')))
            self.stdout.write(red("\tCourse: %s\n" % unicode(subject.module.get_course()).encode('utf-8')))
            
            # Getting fields from XML file
            xml_syllabuses = xml_subject.getElementsByTagName('syllabuses')[0]
            for xml_syllabus in xml_syllabuses.getElementsByTagName('syllabus'):
                teacher_id = xml_syllabus.getElementsByTagName('teacher')[0].getAttribute('id')
                is_published = True if xml_syllabus.getElementsByTagName('is_published')[0].firstChild.nodeValue == '1' else False
                xml_additional_name = xml_syllabus.getElementsByTagName('additional_name')[0]
                additional_name = xml_additional_name.firstChild.nodeValue if xml_additional_name.firstChild else ""
                subject_difficulty_id = xml_syllabus.getElementsByTagName('subject_difficulty')[0].getAttribute('id')
                xml_initial_requirements = xml_syllabus.getElementsByTagName('initial_requirements')[0]
                initial_requirements = xml_initial_requirements.firstChild.nodeValue if xml_initial_requirements.firstChild else ''
                xml_subjects_scope_child_nodes = xml_syllabus.getElementsByTagName('subjects_scope')[0].childNodes
                subjects_scope = ''.join([x.toxml() for x in xml_subjects_scope_child_nodes])
                xml_literature_child_nodes = xml_syllabus.getElementsByTagName('literature')[0].childNodes
                literature = ''.join([x.toxml() for x in xml_literature_child_nodes])
                additional_remarks = xml_syllabus.getElementsByTagName('additional_remarks')[0].firstChild.nodeValue
                
                ects_classes_name = ECTS.objects.get(pk = 4)
                ects_classes_hours = xml_syllabus.getElementsByTagName('ects_classes')[0].getAttribute('value')
                ects_classes_hours = ects_classes_hours.replace(',', '.')
                ects_contact_teacher_name = ECTS.objects.get(pk = 5)
                ects_contact_teacher_hours = xml_syllabus.getElementsByTagName('ects_contact_teacher')[0].getAttribute('value')
                ects_contact_teacher_hours = ects_contact_teacher_hours.replace(',', '.')
                ects_prepare_for_classes_name = ECTS.objects.get(pk = 6)
                ects_prepare_for_classes_hours = xml_syllabus.getElementsByTagName('ects_prepare_for_classes')[0].getAttribute('value')
                ects_prepare_for_classes_hours = ects_prepare_for_classes_hours.replace(',', '.')
                ects_self_study_name = ECTS.objects.get(pk = 7)
                ects_self_study_hours = xml_syllabus.getElementsByTagName('ects_self_study')[0].getAttribute('value')
                ects_self_study_hours = ects_self_study_hours.replace(',', '.')
                ects_prepare_for_exam_name = ECTS.objects.get(pk = 8)
                ects_prepare_for_exam_hours = xml_syllabus.getElementsByTagName('ects_prepare_for_exam')[0].getAttribute('value')
                ects_prepare_for_exam_hours = ects_prepare_for_exam_hours.replace(',', '.')
                
                # Creating objects of fields with foreign key
                teacher = Teacher.objects.get(id = teacher_id)
                subject_difficulty = SubjectDifficulty.objects.get(id = subject_difficulty_id)
                
                # Creating new syllabus subject object
                try:
                    ss = SyllabusSubject.objects.get(subject = subject, teacher = teacher)
                    ss.delete()
                except:
                    pass
                ss = SyllabusSubject()
                ss.subject = subject
                
                # Adding values to new syllabus subject object
                ss.teacher = teacher
                ss.is_published = is_published
                ss.additional_name = additional_name
                ss.subject_difficulty = subject_difficulty
                ss.initial_requirements = initial_requirements
                ss.subjects_scope = subjects_scope
                ss.literature = literature
                ss.additional_information = additional_remarks

                self.stdout.write(yellow("\tTeacher: %s\n" % unicode(ss.teacher).encode('utf-8')))
                self.stdout.write(yellow("\tIs published: %s\n" % unicode(ss.is_published).encode('utf-8')))
                self.stdout.write(yellow("\tAdditional name: %s\n" % unicode(ss.additional_name).encode('utf-8')))
                self.stdout.write(yellow("\tSubject difficulty: %s\n" % unicode(ss.subject_difficulty).encode('utf-8')))
                self.stdout.write(yellow("\tInitial requirements: %s\n" % unicode(ss.initial_requirements).encode('utf-8')))
                self.stdout.write(yellow("\tSubject scope: %s\n" % unicode(ss.subjects_scope).encode('utf-8')))
                self.stdout.write(yellow("\tLiterature: %s\n" % unicode(ss.literature).encode('utf-8')))
                self.stdout.write(yellow("\tAdditional information: %s\n" % unicode(ss.additional_information).encode('utf-8')))

                # Saving new syllabus subject object
                ss.save()
                
                xml_assessment_forms = xml_syllabus.getElementsByTagName('assessment_forms')[0]
                self.stdout.write(yellow("\tAssessment forms: \n"))
                for xml_assessment_form in xml_assessment_forms.getElementsByTagName('assessment_form'):
                    assessment_form_id = xml_assessment_form.getAttribute('id')
                    af = AssessmentForm.objects.get(id = assessment_form_id)
                    ss.assessment_forms.add(af)
                    self.stdout.write(cyan("\t\t%s\n" % unicode(af.name).encode('utf-8')))

                xml_didactic_methods = xml_syllabus.getElementsByTagName('didactic_methods')[0]
                self.stdout.write(yellow("\tDidactic methods: \n"))
                for xml_didactic_method in xml_didactic_methods.getElementsByTagName('didactic_method'):
                    didactic_method_id = xml_didactic_method.getAttribute('id')
                    dm = DidacticMethodMinor.objects.get(id = didactic_method_id)
                    ss.didactic_methods.add(dm)
                    self.stdout.write(cyan("\t\t%s\n" % unicode(dm.name).encode('utf-8')))
                    
                # ECTS hours equivalence
                if subject.ects != 0:
                    ste = SyllabusToECTS()
                    ste.syllabus = ss
                    ste.ects = ects_classes_name
                    ste.hours = ects_classes_hours
                    ste.save()

                    ste = SyllabusToECTS()
                    ste.syllabus = ss
                    ste.ects = ects_contact_teacher_name
                    ste.hours = ects_contact_teacher_hours
                    ste.save()

                    ste = SyllabusToECTS()
                    ste.syllabus = ss
                    ste.ects = ects_prepare_for_classes_name
                    ste.hours = ects_prepare_for_classes_hours
                    ste.save()

                    ste = SyllabusToECTS()
                    ste.syllabus = ss
                    ste.ects = ects_self_study_name
                    ste.hours = ects_self_study_hours
                    ste.save()

                    ste = SyllabusToECTS()
                    ste.syllabus = ss
                    ste.ects = ects_prepare_for_exam_name
                    ste.hours = ects_prepare_for_exam_hours
                    ste.save()