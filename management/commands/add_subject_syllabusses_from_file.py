# -*- coding: utf-8 -*-
'''
Created on 27-08-2012

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.db import transaction

from apps.merovingian.models import Subject
from apps.trainman.models import Teacher
from apps.metacortex.models import SyllabusSubject, SubjectDifficulty, AssessmentForm, DidacticMethodMinor

import csv

from django.utils import termcolors
green = termcolors.make_style(fg='green')
yellow = termcolors.make_style(fg='yellow')
cyan = termcolors.make_style(fg='cyan')
red = termcolors.make_style(fg='red', opts=('bold',))
bold = termcolors.make_style(opts=('bold',))

class Command(BaseCommand):
    help = u'Dodaje sylabusy modułu z pliku.'

    @transaction.commit_on_success
    def handle(self, *args, **options):
        
        # Usunięcie błędnych sylabusów
        try:
            for i in range(3561, 3578):
                syllabus = SyllabusSubject.objects.get(id = i)
                syllabus.delete()
                self.stdout.write(green(u"Sylabus przedmiotu o id: %s został usunięty\n" % unicode(i)))
        except:
            self.stdout.write(green(u"Wystąpił błąd usuwania sylabusów\n"))
    
        ifile  = open('data/mat/przedmioty.csv', "rb")
        reader = csv.reader(ifile)
        reader.next() # Pominięcie nagłówka
        
        iteration = 0
        for row in reader:
            # Znalezienie przedmiotu
            try:
                subject = Subject.objects.get(id = row[2])
                self.stdout.write(green(u"Znaleziono przedmiot %s:\n" % unicode(row[2])))
                self.stdout.write("%s\n" % unicode(subject))
            except Subject.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono przedmiotu %s\n" % unicode(row[2])))

            # Znalezienie prowadzącego
            try:
                teacher = Teacher.objects.get(id = row[3])
                self.stdout.write(green(u"Znaleziono prowadzącego %s:\n" % unicode(row[3])))
                self.stdout.write("%s\n" % unicode(teacher))
            except Teacher.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono prowadzącego %s\n" % unicode(row[3])))
            
            # Znalezienie poziomu przedmiotu
            try:
                subject_difficulty = SubjectDifficulty.objects.get(id = row[5])
                self.stdout.write(green(u"Znaleziono poziom przedmiotu %s:\n" % unicode(row[5])))
                self.stdout.write("%s\n" % unicode(subject_difficulty))
            except SubjectDifficulty.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono poziomu przedmiotu %s\n" % unicode(row[5])))
            
            # Znalezienie form oceniania
            try:
                assessment_form_ids = row[6].split(':')
                assessment_forms = []
                for assessment_form_id in assessment_form_ids:
                    assessment_form = AssessmentForm.objects.get(id = assessment_form_id)
                    assessment_forms.append(assessment_form)
                    self.stdout.write(green(u"Znaleziono formę oceniania %s:\n" % unicode(assessment_form_id)))
                    self.stdout.write("%s\n" % unicode(assessment_form))
            except AssessmentForm.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono formy oceniania %s\n" % unicode(row[6])))
            
            # Znalezienie metod dydaktycznych
            try:
                didactic_methods_ids = row[7].split(':')
                didactic_methods = []
                for didactic_method_id in didactic_methods_ids:
                    didactic_method = DidacticMethodMinor.objects.get(id = didactic_method_id)
                    didactic_methods.append(didactic_method)
                    self.stdout.write(green(u"Znaleziono metodę dydaktyczną %s: " % unicode(didactic_method_id)))
                    self.stdout.write("%s\n" % unicode(didactic_method))
            except DidacticMethodMinor.DoesNotExist:
                self.stdout.write(red(u"Nie znaleziono metody dydaktycznej %s\n" % unicode(row[7])))
            
            # Pobranie dodatkowych informacji
            additional_information = row[11]
            self.stdout.write(yellow("Dodatkowe informacje:\n"))
            self.stdout.write("%s\n" % additional_information)
            
            # Pobranie zakresu tematów
            file_name = row[9]
            f = open('data/mat/ref/'+file_name, 'r')
            file_content = f.read()
            f.close()
            
            subjects_scope = file_content.split('[Zakres tematow]')[1].split('[Efekty.wiedza]')[0].strip().replace('\n','<br/>')
            self.stdout.write(yellow("Zakres tematów:\n"))
            self.stdout.write("%s\n" % subjects_scope) 
            
            # Pobranie literatury
            file_name = row[10]
            f = open('data/mat/ref/'+file_name, 'r')
            file_content = f.read()
            f.close()
            
            literature = file_content.split('[Literatura]')[1].split('[Koniec]')[0].strip().replace('\n','<br/>')
            self.stdout.write(yellow("literatura:\n"))
            self.stdout.write("%s\n" % literature)
            
            # Dodanie sylabusa
            (syllabus_subject, created) = SyllabusSubject.objects.get_or_create(subject = subject, teacher = teacher)
            
            if created:
                self.stdout.write(cyan(u"Sylabus modułu został utworzony\n"))
            else:
                self.stdout.write(cyan(u"Sylabus modułu został nadpisany\n"))
            
            syllabus_subject.is_published = True
            syllabus_subject.is_active = True
            
            syllabus_subject.subject_difficulty = subject_difficulty
            syllabus_subject.literature = literature
            syllabus_subject.subjects_scope = subjects_scope
            syllabus_subject.additional_information = additional_information
            
            # Dodanie metod dydaktycznych
            syllabus_subject.didactic_methods.clear()
            for didactic_method in didactic_methods:
                syllabus_subject.didactic_methods.add(didactic_method)
            
            # Dodanie form oceniania
            syllabus_subject.assessment_forms.clear()
            for assessment_form in assessment_forms:
                syllabus_subject.assessment_forms.add(assessment_form)
            
            syllabus_subject.save()
            
            iteration += 1
            self.stdout.write(bold("\n---------------------: %s\n\n" % iteration))