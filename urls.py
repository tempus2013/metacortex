# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns

# DODAJ SYLABUS
urlpatterns = patterns('',
    (r'select-course$', 'apps.metacortex.views.syllabus_add.select_course'),
    (r'select-speciality/(?P<course_id>\d+)$', 'apps.metacortex.views.syllabus_add.select_speciality'),
    (r'select-module/(?P<course_id>\d+)/(?P<speciality_id>\d+)$', 'apps.metacortex.views.syllabus_add.select_module'),
    (r'select-subject/(?P<course_id>\d+)/(?P<speciality_id>\d+)/(?P<module_id>\d+)$', 'apps.metacortex.views.syllabus_add.select_subject'),
    
    (r'add-module-syllabus/(?P<course_id>\d+)/(?P<speciality_id>\d+)/(?P<module_id>\d+)$', 'apps.metacortex.views.syllabus_add.add_module_syllabus'),
    (r'add-subject-syllabus/(?P<course_id>\d+)/(?P<speciality_id>\d+)/(?P<module_id>\d+)/(?P<subject_id>\d+)$', 'apps.metacortex.views.syllabus_add.add_subject_syllabus'),
    (r'add-practice/(?P<course_id>\d+)/(?P<speciality_id>\d+)/(?P<module_id>\d+)/(?P<subject_id>\d+)$', 'apps.metacortex.views.syllabus_add.add_practice'),                       
)

# MOJE SYLABUSY
urlpatterns += patterns('',
    (r'show(?:/(?P<selected_year>\d{4}))?/$', 'apps.metacortex.views.syllabus_my.show'),
    
    (r'syllabus-delete-ects/(?P<selected_year>\d{4})/(?P<syllabus_type>\d+)/(?P<syllabus_id>\d+)/(?P<ects_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_delete_ects'),
    
    (r'syllabus-module-show/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_module_show'),
    (r'syllabus-module-edit/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_module_edit'),
    (r'syllabus-module-delete/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_module_delete'),
    (r'syllabus-module-print/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_module_print'),
    
    (r'syllabus-subject-show/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_subject_show'),
    (r'syllabus-subject-edit/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_subject_edit'),
    (r'syllabus-subject-delete/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_subject_delete'),   
    (r'syllabus-subject-print/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_subject_print'),
    
    (r'syllabus-subject-copy-list/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_subject_copy_list'),
    (r'syllabus-subject-copy/(?P<selected_year>\d{4})/(?P<syllabus_to_id>\d+)/(?P<syllabus_from_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_subject_copy'),
    
    (r'syllabus-practice-show/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_practice_show'),
    (r'syllabus-practice-edit/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_practice_edit'),
    (r'syllabus-practice-delete/(?P<selected_year>\d{4})/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_practice_delete'),
    (r'syllabus-practice-print/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.syllabus_my.syllabus_practice_print'),
    
    (r'copy-from-last-year/(?P<selected_year>\d{4})$', 'apps.metacortex.views.syllabus_my.copy_from_last_year'),
    (r'copy-module-syllbus/(?P<selected_year>\d{4})/(?P<module_id>\d+)$', 'apps.metacortex.views.syllabus_my.copy_module_syllabus'), 
    (r'copy-subject-syllabus/(?P<selected_year>\d{4})/(?P<subject_id>\d+)$', 'apps.metacortex.views.syllabus_my.copy_subject_syllabus'), 
    (r'copy-practice-syllabus/(?P<selected_year>\d{4})/(?P<subject_id>\d+)$', 'apps.metacortex.views.syllabus_my.copy_practice_syllabus'), 
)

# WYSZUKIWARKA SYLABUSÓW
urlpatterns += patterns('',
    (r'search-classic$', 'apps.metacortex.views.search_engine.syllabus_search_classic'),
    (r'show/(?P<syllabus_type>\d+)/(?P<syllabus_id>\d+)$', 'apps.metacortex.views.search_engine.syllabus_show'),                        
)

# PRZEGLĄDARKA SYLABUSÓW
urlpatterns += patterns('',
    (r'^syllabus/select/(?P<course_id>\d+)$', 'apps.metacortex.views.browser.select_semester'),
    (r'^syllabus/browse/(?P<course_id>\d+)/(?P<semester>\d+)$', 'apps.metacortex.views.browser.browse'),
    (r'^syllabus/supervise/(?P<course_id>\d+)/(?P<semester>\d+)$', 'apps.metacortex.views.browser.supervise'),
)

urlpatterns += patterns('',
    (r'^confirm/(?P<selected_year>\d{4})/(?P<syllabus_to_id>\d+)/(?P<syllabus_from_id>\d+)?$', 'apps.metacortex.views.metacortex.confirm'),
    (r'^help$', 'apps.metacortex.views.metacortex.show_help'),               
    (r'', 'apps.metacortex.views.metacortex.index'), 
)