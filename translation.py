from modeltranslation.translator import translator, TranslationOptions
from apps.metacortex.models import MetacortexNews, SyllabusModule, SyllabusSubject, SyllabusPractice, ECTS, PracticeType, LectureLanguage, DidacticMethodMajor, DidacticMethodMinor, AssessmentForm, SubjectDifficulty

class MetacortexNewsTranslationOptions(TranslationOptions):
    fields = ('content',)

class MetacortexSyllabusModuleTranslationOptions(TranslationOptions):
    fields = ('module_description', 'additional_information',)

class MetacortexSyllabusSubjectTranslationOptions(TranslationOptions):
    fields = ('additional_name', 'initial_requirements', 'literature', 'subjects_scope', 'additional_information', 'education_effects')

class MetacortexSyllabusPracticeTranslationOptions(TranslationOptions):
    fields = ('description', 'education_effects', 'additional_information',)

class MetacortexECTSTranslationOptions(TranslationOptions):
    fields = ('name', )

class MetacortexPracticeTypeTranslationOptions(TranslationOptions):
    fields = ('name', )

class MetacortexLectureLanguageTranslationOptions(TranslationOptions):
    fields = ('name', )

class MetacortexDidacticMethodMajorTranslationOptions(TranslationOptions):
    fields = ('name', )

class MetacortexDidacticMethodMinorTranslationOptions(TranslationOptions):
    fields = ('name', )

class MetacortexAssessmentFormTranslationOptions(TranslationOptions):
    fields = ('name', )

class MetacortexSubjectDifficultyTranslationOptions(TranslationOptions):
    fields = ('name', )

translator.register(MetacortexNews, MetacortexNewsTranslationOptions)
translator.register(SyllabusModule, MetacortexSyllabusModuleTranslationOptions)
translator.register(SyllabusSubject, MetacortexSyllabusSubjectTranslationOptions)
translator.register(SyllabusPractice, MetacortexSyllabusPracticeTranslationOptions)
translator.register(ECTS, MetacortexECTSTranslationOptions)
translator.register(PracticeType, MetacortexPracticeTypeTranslationOptions)
translator.register(LectureLanguage, MetacortexLectureLanguageTranslationOptions)
translator.register(DidacticMethodMajor, MetacortexDidacticMethodMajorTranslationOptions)
translator.register(DidacticMethodMinor, MetacortexDidacticMethodMinorTranslationOptions)
translator.register(AssessmentForm, MetacortexAssessmentFormTranslationOptions)
translator.register(SubjectDifficulty, MetacortexSubjectDifficultyTranslationOptions)