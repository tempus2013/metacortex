# -*- coding: utf-8 -*-

from django.contrib import admin
from apps.metacortex.models import SyllabusModule, SyllabusSubject, LectureLanguage, DidacticMethodMajor, \
    DidacticMethodMinor, AssessmentForm, SubjectDifficulty, MetacortexNews, ECTS, PracticeType, SyllabusYear, \
    MetacortexProfile

from django.utils.translation import ugettext_lazy as _

class MetacortexProfileAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'get_email', 'get_departments')
    search_fields = ('^user_profile__user__last_name', '^user_profile__user__username')
    ordering = ('user_profile__user__last_name',)

    filter_vertical = ('departments',)

    def get_email(self, obj):
        return obj.user_profile.user.email
    get_email.short_description = _(u'Email')

    def get_departments(self, obj):
        result = ''
        for m in obj.departments.all():
            result += unicode(m) + '; '
        return result
    get_departments.short_description = _(u'Departments')


class MetacortexNewsAdmin(admin.ModelAdmin):
    list_display = ('content', 'date', 'is_active')


class SyllabusModuleAdmin(admin.ModelAdmin):
    list_display = ('module', 'get_coordinator', 'is_published', 'is_active')
    search_fields = ('^module__name',)
    exclude = ('module', 'coordinator')

    def get_coordinator(self, obj):
        return obj.coordinator
    get_coordinator.short_description = _(u'Coordinator')


class SyllabusSubjectAdmin(admin.ModelAdmin):
    search_fields = ('^subject__name',)
    list_display = ('subject', 'get_module', 'get_teacher', 'is_published', 'is_active')
    exclude = ('teacher', 'subject')
    
    def get_module(self, obj):
        return obj.subject.module
    get_module.short_description = _(u'Module')
    
    def get_teacher(self, obj):
        return obj.teacher
    get_teacher.short_description = _(u'Teacher')


class SyllabusYearAdmin(admin.ModelAdmin):
    list_display = ('get_date', 'read_only')
    
    def get_date(self, obj):
        return obj.date.year
    get_date.short_description = _(u'Year')

admin.site.register(LectureLanguage)
admin.site.register(DidacticMethodMajor)
admin.site.register(DidacticMethodMinor)
admin.site.register(AssessmentForm)
admin.site.register(SubjectDifficulty)
admin.site.register(MetacortexNews, MetacortexNewsAdmin)
admin.site.register(ECTS)
admin.site.register(SyllabusModule, SyllabusModuleAdmin)
admin.site.register(SyllabusSubject, SyllabusSubjectAdmin)
admin.site.register(PracticeType)
admin.site.register(SyllabusYear, SyllabusYearAdmin)
admin.site.register(MetacortexProfile, MetacortexProfileAdmin)
