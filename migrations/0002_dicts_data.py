# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

from django.utils import translation
import syjon
from django.conf import settings

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Don't use "from appname.models import ModelName". 
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.

        translation.activate('en')

        # ----------------------------------------------
        # --- Practice type
        # ----------------------------------------------

        if len(orm['metacortex.PracticeType'].objects.filter(name_en='trip')) == 0:
            obj = orm['metacortex.PracticeType']()
            obj.name = u'trip'
            obj.name_pl = u'wycieczka'
            obj.name_en = u'trip'
            obj.name_ua = u'поїздка'
            obj.name_ru = u'поездка'
            obj.save()

        if len(orm['metacortex.PracticeType'].objects.filter(name_en='professional practice')) == 0:
            obj = orm['metacortex.PracticeType']()
            obj.name = u'professional practice'
            obj.name_pl = u'praktyka zawodowa'
            obj.name_en = u'professional practice'
            obj.name_ua = u'професійна практика'
            obj.name_ru = u'профессиональная практика'
            obj.save()

        if len(orm['metacortex.PracticeType'].objects.filter(name_en='practice in the field')) == 0:
            obj = orm['metacortex.PracticeType']()
            obj.name = u'practice in the field'
            obj.name_pl = u'praktyka terenowa'
            obj.name_en = u'practice in the field'
            obj.name_ua = u'польова практика'
            obj.name_ru = u'полевая практика'
            obj.save()

        if len(orm['metacortex.PracticeType'].objects.filter(name_en='interim practice')) == 0:
            obj = orm['metacortex.PracticeType']()
            obj.name = u'interim practice'
            obj.name_pl = u'praktyka śródroczna'
            obj.name_en = u'interim practice'
            obj.name_ua = u'практика під час навчального року'
            obj.name_ru = u'практика во время учебного года'
            obj.save()

        if len(orm['metacortex.PracticeType'].objects.filter(name_en='outdoor practice')) == 0:
            obj = orm['metacortex.PracticeType']()
            obj.name = u'outdoor practice'
            obj.name_pl = u'praktyka plenerowa'
            obj.name_en = u'outdoor practice'
            obj.name_ua = u'практика назовні'
            obj.name_ru = u'практика вне помещения'
            obj.save()

        if len(orm['metacortex.PracticeType'].objects.filter(name_en='pedagogical practice')) == 0:
            obj = orm['metacortex.PracticeType']()
            obj.name = u'pedagogical practice'
            obj.name_pl = u'praktyka pedagogiczna'
            obj.name_en = u'pedagogical practice'
            obj.name_ua = u'педагогічна практика'
            obj.name_ru = u'педагогическая практика'
            obj.save()

        if len(orm['metacortex.PracticeType'].objects.filter(name_en='continuous practice')) == 0:
            obj = orm['metacortex.PracticeType']()
            obj.name = u'continuous practice'
            obj.name_pl = u'praktyka ciągła'
            obj.name_en = u'continuous practice'
            obj.name_ua = u'тривала практика'
            obj.name_ru = u'непрерывная практика'
            obj.save()

        if len(orm['metacortex.PracticeType'].objects.filter(name_en='practice')) == 0:
            obj = orm['metacortex.PracticeType']()
            obj.name = u'practice'
            obj.name_pl = u'praktyka'
            obj.name_en = u'practice'
            obj.name_ua = u'практика'
            obj.name_ru = u'практика'
            obj.save()

        if len(orm['metacortex.PracticeType'].objects.filter(name_en='trip')) == 0:
            obj = orm['metacortex.PracticeType']()
            obj.name = u'trip'
            obj.name_pl = u'wycieczka'
            obj.name_en = u'trip'
            obj.name_ua = u'поїздка'
            obj.name_ru = u'поездка'
            obj.save()

        # ----------------------------------------------
        # --- Lecture language
        # ----------------------------------------------

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='romanian')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'romanian'
            obj.name_pl = u'rumuński'
            obj.name_en = u'romanian'
            obj.name_ua = u'румунська'
            obj.name_ru = u'румынский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='greek')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'greek'
            obj.name_pl = u'grecki'
            obj.name_en = u'greek'
            obj.name_ua = u'грецька'
            obj.name_ru = u'греческий'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='serbian')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'serbian'
            obj.name_pl = u'serbski'
            obj.name_en = u'serbian'
            obj.name_ua = u'сербська'
            obj.name_ru = u'сербский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='bulgarian')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'bulgarian'
            obj.name_pl = u'bułgarski'
            obj.name_en = u'bulgarian'
            obj.name_ua = u'болгарська'
            obj.name_ru = u'болгарский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='dutch')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'dutch'
            obj.name_pl = u'holenderski'
            obj.name_en = u'dutch'
            obj.name_ua = u'голландська'
            obj.name_ru = u'голландский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='swedish')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'swedish'
            obj.name_pl = u'szwedzki'
            obj.name_en = u'swedish'
            obj.name_ua = u'шведська'
            obj.name_ru = u'шведский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='slovak')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'slovak'
            obj.name_pl = u'słowacki'
            obj.name_en = u'slovak'
            obj.name_ua = u'словацька'
            obj.name_ru = u'словацкий'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='czech')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'czech'
            obj.name_pl = u'czeski'
            obj.name_en = u'czech'
            obj.name_ua = u'чеська'
            obj.name_ru = u'ческий'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='ukrainian')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'ukrainian'
            obj.name_pl = u'ukraiński'
            obj.name_en = u'ukrainian'
            obj.name_ua = u'українська'
            obj.name_ru = u'украинский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='belarussian')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'belarussian'
            obj.name_pl = u'białoruski'
            obj.name_en = u'belarussian'
            obj.name_ua = u'білоруська'
            obj.name_ru = u'белорусский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='italian')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'italian'
            obj.name_pl = u'włoski'
            obj.name_en = u'italian'
            obj.name_ua = u'італійська'
            obj.name_ru = u'италийский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='portuguese')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'portuguese'
            obj.name_pl = u'portugalski'
            obj.name_en = u'portuguese'
            obj.name_ua = u'португальська'
            obj.name_ru = u'португальский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='spanish')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'spanish'
            obj.name_pl = u'hiszpański'
            obj.name_en = u'spanish'
            obj.name_ua = u'іспанська'
            obj.name_ru = u'испанский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='french')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'french'
            obj.name_pl = u'francuski'
            obj.name_en = u'french'
            obj.name_ua = u'французька'
            obj.name_ru = u'французский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='russian')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'russian'
            obj.name_pl = u'rosyjski'
            obj.name_en = u'russian'
            obj.name_ua = u'російська'
            obj.name_ru = u'русский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='german')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'german'
            obj.name_pl = u'niemiecki'
            obj.name_en = u'german'
            obj.name_ua = u'німецька'
            obj.name_ru = u'немецкий'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='english')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'english'
            obj.name_pl = u'angielski'
            obj.name_en = u'english'
            obj.name_ua = u'англійська'
            obj.name_ru = u'английский'
            obj.save()

        if len(orm['metacortex.LectureLanguage'].objects.filter(name_en='polish')) == 0:
            obj = orm['metacortex.LectureLanguage']()
            obj.name = u'polish'
            obj.name_pl = u'polski'
            obj.name_en = u'polish'
            obj.name_ua = u'польська'
            obj.name_ru = u'польский'
            obj.save()

        # ----------------------------------------------
        # --- Didactic methods
        # ----------------------------------------------

        if len(orm['metacortex.DidacticMethodMajor'].objects.filter(name_en='activation methods')) == 0:
            major = orm['metacortex.DidacticMethodMajor']()
            major.name = u'activation methods'
            major.name_pl = u'metody aktywizujące'
            major.name_en = u'activation methods'
            major.name_ua = u'активізаційні методи'
            major.name_ru = u'активизационные методы'
            major.order = 0
            major.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='creative self expression')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'creative self expression'
            minor.name_pl = u'autoekspresja twórcza'
            minor.name_en = u'creative self expression'
            minor.name_ua = u'творче самовираження'
            minor.name_ru = u'творческое самовыражение'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='didactic discussion')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'didactic discussion'
            minor.name_pl = u'dyskusja dydaktyczna'
            minor.name_en = u'didactic discussion'
            minor.name_ua = u'дидактична дискусія'
            minor.name_ru = u'дидактическая дискуссия'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='film')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'film'
            minor.name_pl = u'film'
            minor.name_en = u'film'
            minor.name_ua = u'фільм'
            minor.name_ru = u'фильм'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='method of case')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'method of case'
            minor.name_pl = u'metoda przypadków'
            minor.name_en = u'method of case'
            minor.name_ua = u'метод вивчення випадку'
            minor.name_ru = u'метод изучения случая'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='seminar')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'seminar'
            minor.name_pl = u'seminarium'
            minor.name_en = u'seminar'
            minor.name_ua = u'Семінар-дискусія'
            minor.name_ru = u'семинар-дискуссия'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='show')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'show'
            minor.name_pl = u'pokaz'
            minor.name_en = u'show'
            minor.name_ua = u'показ'
            minor.name_ru = u'показ'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='situational method')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'situational method'
            minor.name_pl = u'metoda sytuacyjna'
            minor.name_en = u'situational method'
            minor.name_ua = u'ситуаційний метод'
            minor.name_ru = u'ситуационный метод'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='staging')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'staging'
            minor.name_pl = u'inscenizacja'
            minor.name_en = u'staging'
            minor.name_ua = u'інсценізація'
            minor.name_ru = u'инсценизация'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='theater play')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'theater play'
            minor.name_pl = u'sztuka teatralna'
            minor.name_en = u'theater play'
            minor.name_ua = u'рольова гра'
            minor.name_ru = u'ролевая игра'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='work correction')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'work correction'
            minor.name_pl = u'korekta prac'
            minor.name_en = u'work correction'
            minor.name_ua = u'перевірка робіт'
            minor.name_ru = u'проверка работ'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMajor'].objects.filter(name_en='exposing methods')) == 0:
            major = orm['metacortex.DidacticMethodMajor']()
            major.name = u'exposing methods'
            major.name_pl = u'metody eksponujące'
            major.name_en = u'exposing methods'
            major.name_ua = u'експозиційні методи'
            major.name_ru = u'экспозиционные методы'
            major.order = 0
            major.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='exposure')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'exposure'
            minor.name_pl = u'ekspozycja'
            minor.name_en = u'exposure'
            minor.name_ua = u'експозиція'
            minor.name_ru = u'экспозиция'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMajor'].objects.filter(name_en='giving methods')) == 0:
            major = orm['metacortex.DidacticMethodMajor']()
            major.name = u'giving methods'
            major.name_pl = u'metody podające'
            major.name_en = u'giving methods'
            major.name_ua = u'даючи методи'
            major.name_ru = u'давая методы'
            major.order = 0
            major.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='consultations')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'consultations'
            minor.name_pl = u'konsultacje'
            minor.name_en = u'consultations'
            minor.name_ua = u'консультації'
            minor.name_ru = u'консультации'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='description')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'description'
            minor.name_pl = u'opis'
            minor.name_en = u'description'
            minor.name_ua = u'опис'
            minor.name_ru = u'описание'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='explanation or clarification')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'explanation or clarification'
            minor.name_pl = u'objaśnienie lub wyjaśnienie'
            minor.name_en = u'explanation or clarification'
            minor.name_ua = u'пояснення чи кларифікація'
            minor.name_ru = u'объяснение или разъяснение'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='informative lecture')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'informative lecture'
            minor.name_pl = u'wykład informacyjny'
            minor.name_en = u'informative lecture'
            minor.name_ua = u'інформативна лекція'
            minor.name_ru = u'информативная лекция '
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='lecture')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'lecture'
            minor.name_pl = u'prelekcja'
            minor.name_en = u'lecture'
            minor.name_ua = u'лекція'
            minor.name_ru = u'лекция'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='reading')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'reading'
            minor.name_pl = u'odczyt'
            minor.name_en = u'reading'
            minor.name_ua = u'читання'
            minor.name_ru = u'чтение'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='story')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'story'
            minor.name_pl = u'opowiadanie'
            minor.name_en = u'story'
            minor.name_ua = u'оповідання'
            minor.name_ru = u'рассказ'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMajor'].objects.filter(name_en='other')) == 0:
            major = orm['metacortex.DidacticMethodMajor']()
            major.name = u'other'
            major.name_pl = u'inne'
            major.name_en = u'other'
            major.name_ua = u'інші'
            major.name_ru = u'другие'
            major.order = 0
            major.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='e-learning')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'e-learning'
            minor.name_pl = u'e-learning'
            minor.name_en = u'e-learning'
            minor.name_ua = u'Е-навчання'
            minor.name_ru = u'электронное обучение'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='methods of teaching motor function')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'methods of teaching motor function'
            minor.name_pl = u'metody nauczania czynności ruchowych'
            minor.name_en = u'methods of teaching motor function'
            minor.name_ua = u'методи навчання рухових фунцій'
            minor.name_ru = u'методы обучения двигательных функций'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMajor'].objects.filter(name_en='practical methods')) == 0:
            major = orm['metacortex.DidacticMethodMajor']()
            major.name = u'practical methods'
            major.name_pl = u'metody praktyczne'
            major.name_en = u'practical methods'
            major.name_ua = u'практичні методи'
            major.name_ru = u'практические методы'
            major.order = 0
            major.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='group workshops')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'group workshops'
            minor.name_pl = u'warsztaty grupowe'
            minor.name_en = u'group workshops'
            minor.name_ua = u'груповий воркшоп'
            minor.name_ru = u'групповой воркшоп'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='laboratory exercises')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'laboratory exercises'
            minor.name_pl = u'ćwiczenia laboratoryjne'
            minor.name_en = u'laboratory exercises'
            minor.name_ua = u'лабораторні вправи'
            minor.name_ru = u'лабораторные упражнения'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='manufacturing practice')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'manufacturing practice'
            minor.name_pl = u'ćwiczenia produkcyjne'
            minor.name_en = u'manufacturing practice'
            minor.name_ua = u'продукційні вправи'
            minor.name_ru = u'продукционные упражнения'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='method of guiding text')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'method of guiding text'
            minor.name_pl = u'metoda przewodniego tekstu'
            minor.name_en = u'method of guiding text'
            minor.name_ua = u'метод ведучого тексту'
            minor.name_ru = u'метод ведущего текста'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='projects and field work')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'projects and field work'
            minor.name_pl = u'projekty i prace terenowe'
            minor.name_en = u'projects and field work'
            minor.name_ua = u'проекти і польові семінари'
            minor.name_ru = u'проекты и полевые семинары'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='projects method')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'projects method'
            minor.name_pl = u'metoda projektów'
            minor.name_en = u'projects method'
            minor.name_ua = u'метод проектів'
            minor.name_ru = u'метод проектов'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='seminar')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'seminar'
            minor.name_pl = u'seminarium'
            minor.name_en = u'seminar'
            minor.name_ua = u'Семінар-дискусія'
            minor.name_ru = u'семинар-дискуссия'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='show')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'show'
            minor.name_pl = u'pokaz'
            minor.name_en = u'show'
            minor.name_ua = u'показ'
            minor.name_ru = u'показ'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='simulation')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'simulation'
            minor.name_pl = u'symulacja'
            minor.name_en = u'simulation'
            minor.name_ua = u'симуляція'
            minor.name_ru = u'симуляция'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='subject exercise')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'subject exercise'
            minor.name_pl = u'ćwiczenia przedmiotowe'
            minor.name_en = u'subject exercise'
            minor.name_ua = u'предметні вправи'
            minor.name_ru = u'предметные упражнения'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMajor'].objects.filter(name_en='problem methods')) == 0:
            major = orm['metacortex.DidacticMethodMajor']()
            major.name = u'problem methods'
            major.name_pl = u'metody problemowe'
            major.name_en = u'problem methods'
            major.name_ua = u'Проблемні методи'
            major.name_ru = u'проблемные методы'
            major.order = 0
            major.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='classic method of problem')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'classic method of problem'
            minor.name_pl = u'klasyczna metoda problemowa'
            minor.name_en = u'classic method of problem'
            minor.name_ua = u'класичний проблемний метод'
            minor.name_ru = u'классический проблемный метод'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='discussion seminar')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'discussion seminar'
            minor.name_pl = u'wykład konwersatoryjny'
            minor.name_en = u'discussion seminar'
            minor.name_ua = u'Семінар-дискусія'
            minor.name_ru = u'семинар-дискуссия'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='problem seminar')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'problem seminar'
            minor.name_pl = u'wykład problemowy'
            minor.name_en = u'problem seminar'
            minor.name_ua = u'проблемний семінар'
            minor.name_ru = u'проблемный семинар'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMajor'].objects.filter(name_en='programmed methods')) == 0:
            major = orm['metacortex.DidacticMethodMajor']()
            major.name = u'programmed methods'
            major.name_pl = u'metody programowane'
            major.name_en = u'programmed methods'
            major.name_ua = u'програмовані методи'
            major.name_ru = u'програмированные методы'
            major.order = 0
            major.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='with computer')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'with computer'
            minor.name_pl = u'z użyciem komputera'
            minor.name_en = u'with computer'
            minor.name_ua = u'з компютером'
            minor.name_ru = u'с компъютером'
            minor.didactic_method = major
            minor.order = 0
            minor.save()

        if len(orm['metacortex.DidacticMethodMinor'].objects.filter(name_en='with manual')) == 0:
            minor = orm['metacortex.DidacticMethodMinor']()
            minor.name = u'with manual'
            minor.name_pl = u'z użyciem podręcznika programowanego'
            minor.name_en = u'with manual'
            minor.name_ua = u'з інструкцією'
            minor.name_ru = u'с инструкцией'
            minor.didactic_method = major
            minor.order = 0
            minor.save()


        # ----------------------------------------------
        # --- Assessment form
        # ----------------------------------------------

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='annual paper')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'annual paper'
            obj.name_pl = u'praca roczna'
            obj.name_en = u'annual paper'
            obj.name_ua = u'річний проект'
            obj.name_ru = u'годичный проект'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='attendance')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'attendance'
            obj.name_pl = u'obecność na zajęciach'
            obj.name_en = u'attendance'
            obj.name_ua = u'відвідуваність'
            obj.name_ru = u'посещаемость'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='continuous assessment (preparation for classes and activities)')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'continuous assessment (preparation for classes and activities)'
            obj.name_pl = u'ocena ciągła (bieżące przygotowanie do zajęć i aktywność)'
            obj.name_en = u'continuous assessment (preparation for classes and activities)'
            obj.name_ua = u'оцінювання на підставі приготування до занять і активності'
            obj.name_ru = u'оценивание на основе подготовки к занятиям и активности'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='dissertation')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'dissertation'
            obj.name_pl = u'dysertacja'
            obj.name_en = u'dissertation'
            obj.name_ua = u'дисертація'
            obj.name_ru = u'диссертация'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='documentation of a project')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'documentation of a project'
            obj.name_pl = u'dokumentacja realizacji projektu'
            obj.name_en = u'documentation of a project'
            obj.name_ua = u'документація проекту'
            obj.name_ru = u'документация проекта'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='essay')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'essay'
            obj.name_pl = u'esej'
            obj.name_en = u'essay'
            obj.name_ua = u'ессе'
            obj.name_ru = u'эссе'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='exhibition')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'exhibition'
            obj.name_pl = u'wystawa'
            obj.name_en = u'exhibition'
            obj.name_ua = u'виставка'
            obj.name_ru = u'выставка'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='final exhibition')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'final exhibition'
            obj.name_pl = u'wystawa końcoworoczna'
            obj.name_en = u'final exhibition'
            obj.name_ua = u'річна виставка '
            obj.name_ru = u'годичная выставка'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='final oral exam')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'final oral exam'
            obj.name_pl = u'końcowe zaliczenie ustne'
            obj.name_en = u'final oral exam'
            obj.name_ua = u'річний усний іспит'
            obj.name_ru = u'годичный устный экзамен'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='final written exam')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'final written exam'
            obj.name_pl = u'końcowe zaliczenie pisemne'
            obj.name_en = u'final written exam'
            obj.name_ua = u'річний письмовий іспит'
            obj.name_ru = u'годичный письменный экзамен'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='midterm exhibition')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'midterm exhibition'
            obj.name_pl = u'wystawa semestralna'
            obj.name_en = u'midterm exhibition'
            obj.name_ua = u'дипломна робота'
            obj.name_ru = u'дипломная работа'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='midterm oral colloquia')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'midterm oral colloquia'
            obj.name_pl = u'śródsemestralne ustne kolokwia'
            obj.name_en = u'midterm oral colloquia'
            obj.name_ua = u'усний колоквіум'
            obj.name_ru = u'устный коллоквиум'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='midterm written control tests')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'midterm written control tests'
            obj.name_pl = u'śródsemestralne pisemne testy kontrolne'
            obj.name_en = u'midterm written control tests'
            obj.name_ua = u'письмовий тест'
            obj.name_ru = u'письменный текст'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='MSc thesis')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'MSc thesis'
            obj.name_pl = u'praca magisterska'
            obj.name_en = u'MSc thesis'
            obj.name_ua = u'магістерська робота'
            obj.name_ru = u'магистерская работа'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='oral exam')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'oral exam'
            obj.name_pl = u'egzamin ustny'
            obj.name_en = u'oral exam'
            obj.name_ua = u'усний іспит'
            obj.name_ru = u'устный экзамен'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='other')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'other'
            obj.name_pl = u'inne'
            obj.name_en = u'other'
            obj.name_ua = u'інше'
            obj.name_ru = u'другое'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='overview of the work')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'overview of the work'
            obj.name_pl = u'przegląd prac'
            obj.name_en = u'overview of the work'
            obj.name_ua = u'перегляд робіт'
            obj.name_ru = u'обзор работ'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='portfolio')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'portfolio'
            obj.name_pl = u'portfolio'
            obj.name_en = u'portfolio'
            obj.name_ua = u'портфоліо'
            obj.name_ru = u'портфолио'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='practical exercises/laboratory')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'practical exercises/laboratory'
            obj.name_pl = u'ćwiczenia praktyczne/laboratoryjne'
            obj.name_en = u'practical exercises/laboratory'
            obj.name_ua = u'практичні/лабораторні заняття'
            obj.name_ru = u'практические/лабораторные занятия'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='practical pass')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'practical pass'
            obj.name_pl = u'zaliczenie praktyczne'
            obj.name_en = u'practical pass'
            obj.name_ua = u'письмовий залік'
            obj.name_ru = u'письменный зачет'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='project')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'project'
            obj.name_pl = u'projekt'
            obj.name_en = u'project'
            obj.name_ua = u'проект на кінець семестру'
            obj.name_ru = u'проект на конец семестра'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='project')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'project'
            obj.name_pl = u'realizacja projektu'
            obj.name_en = u'project'
            obj.name_ua = u'реалізація проекту'
            obj.name_ru = u'реализация проекта'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='report')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'report'
            obj.name_pl = u'referat'
            obj.name_en = u'report'
            obj.name_ua = u'реферат'
            obj.name_ru = u'реферат'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='term paper')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'term paper'
            obj.name_pl = u'praca semestralna'
            obj.name_en = u'term paper'
            obj.name_ua = u'проект на кінець семестру'
            obj.name_ru = u'проект на конец семестра'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='thesis')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'thesis'
            obj.name_pl = u'praca dyplomowa'
            obj.name_en = u'thesis'
            obj.name_ua = u'курсова'
            obj.name_ru = u'курсовая'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.AssessmentForm'].objects.filter(name_en='written exam')) == 0:
            obj = orm['metacortex.AssessmentForm']()
            obj.name = u'written exam'
            obj.name_pl = u'egzamin pisemny'
            obj.name_en = u'written exam'
            obj.name_ua = u'письмовий іспит'
            obj.name_ru = u'письменный экзамен'
            obj.order = 0
            obj.save()


        # ----------------------------------------------
        # --- Subject difficulty
        # ----------------------------------------------

        if len(orm['metacortex.SubjectDifficulty'].objects.filter(name_en='basic')) == 0:
            obj = orm['metacortex.SubjectDifficulty']()
            obj.name = u'basic'
            obj.name_pl = u'podstawowy'
            obj.name_en = u'basic'
            obj.name_ua = u'базовий'
            obj.name_ru = u'базовый'
            obj.order = 0
            obj.save()

        if len(orm['metacortex.SubjectDifficulty'].objects.filter(name_en='intermediate')) == 0:
            obj = orm['metacortex.SubjectDifficulty']()
            obj.name = u'intermediate'
            obj.name_pl = u'średnio zaawansowany'
            obj.name_en = u'intermediate'
            obj.name_ua = u'середній'
            obj.name_ru = u'средний'
            obj.order = 1
            obj.save()

        if len(orm['metacortex.SubjectDifficulty'].objects.filter(name_en='advanced')) == 0:
            obj = orm['metacortex.SubjectDifficulty']()
            obj.name = u'advanced'
            obj.name_pl = u'zaawansowany'
            obj.name_en = u'advanced'
            obj.name_ua = u'поглиблений'
            obj.name_ru = u'углубленный'
            obj.order = 2
            obj.save()

        if len(orm['metacortex.SubjectDifficulty'].objects.filter(name_en='all levels')) == 0:
            obj = orm['metacortex.SubjectDifficulty']()
            obj.name = u'all levels'
            obj.name_pl = u'wszystkie poziomy'
            obj.name_en = u'all levels'
            obj.name_ua = u'всі рівні'
            obj.name_ru = u'все уровни'
            obj.order = 3
            obj.save()

        if len(orm['metacortex.SubjectDifficulty'].objects.filter(name_en='not applicable')) == 0:
            obj = orm['metacortex.SubjectDifficulty']()
            obj.name = u'not applicable'
            obj.name_pl = u'nie dotyczy'
            obj.name_en = u'not applicable'
            obj.name_ua = u'не стосується'
            obj.name_ru = u'не касается'
            obj.order = 4
            obj.save()


    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'merovingian.course': {
            'Meta': {'ordering': "('-is_active', 'name', '-level__name', '-type__name', 'profile__name', 'start_date')", 'object_name': 'Course', 'db_table': "'merv_course'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'end_date'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_first': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_first'", 'blank': 'True'}),
            'is_last': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_last'", 'blank': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']", 'db_column': "'id_level'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseProfile']", 'null': 'True', 'db_column': "'id_profile'", 'blank': 'True'}),
            'semesters': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'db_column': "'semesters'", 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'start_date'", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseType']", 'db_column': "'id_type'"}),
            'years': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'db_column': "'years'", 'blank': 'True'})
        },
        'merovingian.courselevel': {
            'Meta': {'object_name': 'CourseLevel', 'db_table': "'merv_course_level'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.courseprofile': {
            'Meta': {'object_name': 'CourseProfile', 'db_table': "'merv_course_profile'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.coursetype': {
            'Meta': {'object_name': 'CourseType', 'db_table': "'merv_course_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.didacticoffer': {
            'Meta': {'object_name': 'DidacticOffer', 'db_table': "'merv_didactic_offer'"},
            'end_date': ('django.db.models.fields.DateField', [], {'db_column': "'end_date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'db_column': "'start_date'"})
        },
        'merovingian.module': {
            'Meta': {'ordering': "['name']", 'object_name': 'Module', 'db_table': "'merv_module'"},
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.ModuleType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'merovingian.moduletype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'ModuleType', 'db_table': "'merv_module_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subject': {
            'Meta': {'ordering': "('semester', 'name', '-type', 'assessment')", 'object_name': 'Subject', 'db_table': "'merv_subject'"},
            'assessment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectAssessment']", 'db_column': "'id_assessment'"}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'hours': ('django.db.models.fields.FloatField', [], {'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']", 'db_column': "'id_module'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'semester': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semester'"}),
            'teachers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trainman.Teacher']", 'null': 'True', 'through': "orm['merovingian.SubjectToTeacher']", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectType']", 'db_column': "'id_type'"})
        },
        'merovingian.subjectassessment': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectAssessment', 'db_table': "'merv_subject_assessment'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subjecttoteacher': {
            'Meta': {'object_name': 'SubjectToTeacher', 'db_table': "'merv_subject__to__teacher'"},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'db_column': "'description'", 'blank': 'True'}),
            'groups': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'groups'"}),
            'hours': ('django.db.models.fields.FloatField', [], {'max_length': '3', 'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']", 'db_column': "'id_subject'"}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']", 'db_column': "'id_teacher'"})
        },
        'merovingian.subjecttype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectType', 'db_table': "'merv_subject_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'metacortex.assessmentform': {
            'Meta': {'ordering': "['order', 'name']", 'object_name': 'AssessmentForm', 'db_table': "'metacortex_assessment_form'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'metacortex.didacticmethodmajor': {
            'Meta': {'ordering': "['order', 'name']", 'object_name': 'DidacticMethodMajor', 'db_table': "'metacortex_didactic_method_major'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'metacortex.didacticmethodminor': {
            'Meta': {'ordering': "['order', 'name']", 'object_name': 'DidacticMethodMinor', 'db_table': "'metacortex_didactic_method_minor'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'major_didactic_method': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.DidacticMethodMajor']", 'db_column': "'id_didactic_method_major'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'metacortex.ects': {
            'Meta': {'ordering': "('is_default', 'name')", 'object_name': 'ECTS'},
            'flag': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'metacortex.lecturelanguage': {
            'Meta': {'object_name': 'LectureLanguage', 'db_table': "'metacortex_lecture_language'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        'metacortex.metacortexnews': {
            'Meta': {'ordering': "('-date', '-is_active')", 'object_name': 'MetacortexNews', 'db_table': "'metacortex_news'"},
            'content': ('django.db.models.fields.TextField', [], {}),
            'content_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'content_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'content_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'content_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'db_column': "'date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'metacortex.practicetype': {
            'Meta': {'object_name': 'PracticeType', 'db_table': "'metacortex_practice_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        'metacortex.subjectdifficulty': {
            'Meta': {'ordering': "['order', 'name']", 'object_name': 'SubjectDifficulty', 'db_table': "'metacortex_subject_difficulty'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'metacortex.syllabus': {
            'Meta': {'object_name': 'Syllabus'},
            'ectss': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['metacortex.ECTS']", 'through': "orm['metacortex.SyllabusToECTS']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'metacortex.syllabusmodule': {
            'Meta': {'ordering': "['module__name']", 'object_name': 'SyllabusModule', 'db_table': "'metacortex_syllabus_module'", '_ormbases': ['metacortex.Syllabus']},
            'additional_information': ('django.db.models.fields.TextField', [], {}),
            'additional_information_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'coordinator': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'module_coordinator'", 'null': 'True', 'to': "orm['trainman.Teacher']"}),
            'lecture_languages': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'lecturelanguages'", 'to': "orm['metacortex.LectureLanguage']", 'db_table': "'metacortex_syllabus_module__to__lecture_language'", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'module': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['merovingian.Module']", 'unique': 'True'}),
            'module_description': ('django.db.models.fields.TextField', [], {}),
            'module_description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'module_description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'module_description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'module_description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'syllabus_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metacortex.Syllabus']", 'unique': 'True', 'primary_key': 'True'}),
            'unit_source': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'unit_source'", 'null': 'True', 'to': "orm['trainman.Department']"}),
            'unit_target': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'unit_target'", 'null': 'True', 'to': "orm['trainman.Department']"})
        },
        'metacortex.syllabuspractice': {
            'Meta': {'ordering': "['subject__name']", 'object_name': 'SyllabusPractice', 'db_table': "'metacortex_syllabus_practice'", '_ormbases': ['metacortex.Syllabus']},
            'additional_information': ('django.db.models.fields.TextField', [], {}),
            'additional_information_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects': ('django.db.models.fields.TextField', [], {}),
            'education_effects_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']"}),
            'syllabus_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metacortex.Syllabus']", 'unique': 'True', 'primary_key': 'True'}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.PracticeType']", 'null': 'True', 'blank': 'True'})
        },
        'metacortex.syllabussubject': {
            'Meta': {'ordering': "['subject__name']", 'object_name': 'SyllabusSubject', 'db_table': "'metacortex_syllabus_subject'", '_ormbases': ['metacortex.Syllabus']},
            'additional_information': ('django.db.models.fields.TextField', [], {}),
            'additional_information_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'additional_name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'additional_name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'additional_name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'additional_name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'assessment_forms': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['metacortex.AssessmentForm']", 'db_table': "'metacortex_syllabus_subject__to__assessment_form'", 'symmetrical': 'False'}),
            'didactic_methods': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['metacortex.DidacticMethodMinor']", 'db_table': "'metacortex_syllabus_subject__to__didactic_method'", 'symmetrical': 'False'}),
            'education_effects': ('django.db.models.fields.TextField', [], {}),
            'education_effects_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'initial_requirements': ('django.db.models.fields.TextField', [], {}),
            'initial_requirements_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'initial_requirements_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'initial_requirements_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'initial_requirements_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'literature': ('django.db.models.fields.TextField', [], {}),
            'literature_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'literature_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'literature_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'literature_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']"}),
            'subject_difficulty': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.SubjectDifficulty']", 'null': 'True', 'blank': 'True'}),
            'subjects_scope': ('django.db.models.fields.TextField', [], {}),
            'subjects_scope_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subjects_scope_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subjects_scope_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subjects_scope_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'syllabus_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metacortex.Syllabus']", 'unique': 'True', 'primary_key': 'True'}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']"})
        },
        'metacortex.syllabustoects': {
            'Meta': {'ordering': "('-ects__is_default', 'ects__order', 'ects__name')", 'object_name': 'SyllabusToECTS', 'db_table': "'metacortex_syllabus__to__ects'"},
            'ects': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.ECTS']", 'db_column': "'id_ects'"}),
            'hours': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'syllabus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.Syllabus']", 'db_column': "'id_syllabus'"})
        },
        'metacortex.syllabusyear': {
            'Meta': {'ordering': "['-date']", 'object_name': 'SyllabusYear', 'db_table': "'meatcortex_syllabus_year'"},
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'read_only': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'trainman.department': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Department'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_department'", 'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.DepartmentType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'trainman.departmenttype': {
            'Meta': {'object_name': 'DepartmentType', 'db_table': "'trainman_department_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.teacher': {
            'Meta': {'ordering': "('user_profile__user__last_name',)", 'object_name': 'Teacher'},
            'degree': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherDegree']", 'db_column': "'id_degree'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherPosition']", 'db_column': "'id_position'"}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'user_profile'"})
        },
        'trainman.teacherdegree': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherDegree', 'db_table': "'trainman_teacher_degree'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.teacherposition': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherPosition', 'db_table': "'trainman_teacher_position'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.userprofile': {
            'Meta': {'ordering': "('user__last_name',)", 'object_name': 'UserProfile', 'db_table': "'trainman_user_profile'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pesel': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True', 'db_column': "'pesel'", 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'db_column': "'second_name'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'db_column': "'user'"})
        },
        'trinity.arealearningoutcome': {
            'Meta': {'ordering': "['symbol']", 'object_name': 'AreaLearningOutcome', 'db_table': "'trinity_alo'"},
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_area': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationArea']"}),
            'education_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationCategory']"}),
            'education_level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']"}),
            'education_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseProfile']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'symbol_en': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_pl': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ru': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ua': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        'trinity.courselearningoutcome': {
            'Meta': {'ordering': "['symbol']", 'object_name': 'CourseLearningOutcome', 'db_table': "'trinity_clo'"},
            'alos': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trinity.AreaLearningOutcome']", 'db_table': "'trinity_clo__to__alo'", 'symmetrical': 'False'}),
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Course']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationCategory']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'symbol_en': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_pl': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ru': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ua': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationarea': {
            'Meta': {'ordering': "['name']", 'object_name': 'EducationArea', 'db_table': "'trinity_education_area'"},
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_education_area__to__course'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationcategory': {
            'Meta': {'ordering': "['-name']", 'object_name': 'EducationCategory', 'db_table': "'trinity_education_category'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.modulelearningoutcome': {
            'Meta': {'ordering': "['symbol']", 'object_name': 'ModuleLearningOutcome', 'db_table': "'trinity_mlo'"},
            'clos': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trinity.CourseLearningOutcome']", 'db_table': "'trinity_mlo__to__clo'", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']"}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'symbol_en': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_pl': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ru': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ua': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['metacortex']
    symmetrical = True
