# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MetacortexNews'
        db.create_table('metacortex_news', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('content_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('content_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('content_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('content_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('date', self.gf('django.db.models.fields.DateField')(db_column='date')),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('metacortex', ['MetacortexNews'])

        # Adding model 'Syllabus'
        db.create_table('metacortex_syllabus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('metacortex', ['Syllabus'])

        # Adding model 'SyllabusToECTS'
        db.create_table('metacortex_syllabus__to__ects', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('syllabus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['metacortex.Syllabus'], db_column='id_syllabus')),
            ('ects', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['metacortex.ECTS'], db_column='id_ects')),
            ('hours', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
        ))
        db.send_create_signal('metacortex', ['SyllabusToECTS'])

        # Adding model 'ECTS'
        db.create_table('metacortex_ects', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('is_default', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('order', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('flag', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('metacortex', ['ECTS'])

        # Adding model 'SyllabusModule'
        db.create_table('metacortex_syllabus_module', (
            ('syllabus_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['metacortex.Syllabus'], unique=True, primary_key=True)),
            ('module', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['merovingian.Module'], unique=True)),
            ('module_description', self.gf('django.db.models.fields.TextField')()),
            ('module_description_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('module_description_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('module_description_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('module_description_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('unit_source', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='unit_source', null=True, to=orm['trainman.Department'])),
            ('unit_target', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='unit_target', null=True, to=orm['trainman.Department'])),
            ('coordinator', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='module_coordinator', null=True, to=orm['trainman.Teacher'])),
            ('additional_information', self.gf('django.db.models.fields.TextField')()),
            ('additional_information_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('metacortex', ['SyllabusModule'])

        # Adding M2M table for field lecture_languages on 'SyllabusModule'
        db.create_table('metacortex_syllabus_module__to__lecture_language', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('syllabusmodule', models.ForeignKey(orm['metacortex.syllabusmodule'], null=False)),
            ('lecturelanguage', models.ForeignKey(orm['metacortex.lecturelanguage'], null=False))
        ))
        db.create_unique('metacortex_syllabus_module__to__lecture_language', ['syllabusmodule_id', 'lecturelanguage_id'])

        # Adding model 'SyllabusSubject'
        db.create_table('metacortex_syllabus_subject', (
            ('syllabus_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['metacortex.Syllabus'], unique=True, primary_key=True)),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Subject'])),
            ('subject_difficulty', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['metacortex.SubjectDifficulty'], null=True, blank=True)),
            ('additional_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('additional_name_pl', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('additional_name_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('additional_name_ua', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('additional_name_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('initial_requirements', self.gf('django.db.models.fields.TextField')()),
            ('initial_requirements_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('initial_requirements_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('initial_requirements_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('initial_requirements_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('literature', self.gf('django.db.models.fields.TextField')()),
            ('literature_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('literature_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('literature_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('literature_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('subjects_scope', self.gf('django.db.models.fields.TextField')()),
            ('subjects_scope_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('subjects_scope_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('subjects_scope_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('subjects_scope_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information', self.gf('django.db.models.fields.TextField')()),
            ('additional_information_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_effects', self.gf('django.db.models.fields.TextField')()),
            ('education_effects_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_effects_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_effects_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_effects_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Teacher'])),
        ))
        db.send_create_signal('metacortex', ['SyllabusSubject'])

        # Adding M2M table for field didactic_methods on 'SyllabusSubject'
        db.create_table('metacortex_syllabus_subject__to__didactic_method', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('syllabussubject', models.ForeignKey(orm['metacortex.syllabussubject'], null=False)),
            ('didacticmethodminor', models.ForeignKey(orm['metacortex.didacticmethodminor'], null=False))
        ))
        db.create_unique('metacortex_syllabus_subject__to__didactic_method', ['syllabussubject_id', 'didacticmethodminor_id'])

        # Adding M2M table for field assessment_forms on 'SyllabusSubject'
        db.create_table('metacortex_syllabus_subject__to__assessment_form', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('syllabussubject', models.ForeignKey(orm['metacortex.syllabussubject'], null=False)),
            ('assessmentform', models.ForeignKey(orm['metacortex.assessmentform'], null=False))
        ))
        db.create_unique('metacortex_syllabus_subject__to__assessment_form', ['syllabussubject_id', 'assessmentform_id'])

        # Adding model 'SyllabusPractice'
        db.create_table('metacortex_syllabus_practice', (
            ('syllabus_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['metacortex.Syllabus'], unique=True, primary_key=True)),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Subject'])),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Teacher'])),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['metacortex.PracticeType'], null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('description_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('description_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('description_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('description_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_effects', self.gf('django.db.models.fields.TextField')()),
            ('education_effects_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_effects_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_effects_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_effects_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information', self.gf('django.db.models.fields.TextField')()),
            ('additional_information_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('additional_information_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('metacortex', ['SyllabusPractice'])

        # Adding model 'PracticeType'
        db.create_table('metacortex_practice_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
        ))
        db.send_create_signal('metacortex', ['PracticeType'])

        # Adding model 'LectureLanguage'
        db.create_table('metacortex_lecture_language', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
        ))
        db.send_create_signal('metacortex', ['LectureLanguage'])

        # Adding model 'DidacticMethodMajor'
        db.create_table('metacortex_didactic_method_major', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('metacortex', ['DidacticMethodMajor'])

        # Adding model 'DidacticMethodMinor'
        db.create_table('metacortex_didactic_method_minor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('major_didactic_method', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['metacortex.DidacticMethodMajor'], db_column='id_didactic_method_major')),
        ))
        db.send_create_signal('metacortex', ['DidacticMethodMinor'])

        # Adding model 'AssessmentForm'
        db.create_table('metacortex_assessment_form', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('metacortex', ['AssessmentForm'])

        # Adding model 'SubjectDifficulty'
        db.create_table('metacortex_subject_difficulty', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('metacortex', ['SubjectDifficulty'])

        # Adding model 'SyllabusYear'
        db.create_table('meatcortex_syllabus_year', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('read_only', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('metacortex', ['SyllabusYear'])


    def backwards(self, orm):
        # Deleting model 'MetacortexNews'
        db.delete_table('metacortex_news')

        # Deleting model 'Syllabus'
        db.delete_table('metacortex_syllabus')

        # Deleting model 'SyllabusToECTS'
        db.delete_table('metacortex_syllabus__to__ects')

        # Deleting model 'ECTS'
        db.delete_table('metacortex_ects')

        # Deleting model 'SyllabusModule'
        db.delete_table('metacortex_syllabus_module')

        # Removing M2M table for field lecture_languages on 'SyllabusModule'
        db.delete_table('metacortex_syllabus_module__to__lecture_language')

        # Deleting model 'SyllabusSubject'
        db.delete_table('metacortex_syllabus_subject')

        # Removing M2M table for field didactic_methods on 'SyllabusSubject'
        db.delete_table('metacortex_syllabus_subject__to__didactic_method')

        # Removing M2M table for field assessment_forms on 'SyllabusSubject'
        db.delete_table('metacortex_syllabus_subject__to__assessment_form')

        # Deleting model 'SyllabusPractice'
        db.delete_table('metacortex_syllabus_practice')

        # Deleting model 'PracticeType'
        db.delete_table('metacortex_practice_type')

        # Deleting model 'LectureLanguage'
        db.delete_table('metacortex_lecture_language')

        # Deleting model 'DidacticMethodMajor'
        db.delete_table('metacortex_didactic_method_major')

        # Deleting model 'DidacticMethodMinor'
        db.delete_table('metacortex_didactic_method_minor')

        # Deleting model 'AssessmentForm'
        db.delete_table('metacortex_assessment_form')

        # Deleting model 'SubjectDifficulty'
        db.delete_table('metacortex_subject_difficulty')

        # Deleting model 'SyllabusYear'
        db.delete_table('meatcortex_syllabus_year')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'merovingian.didacticoffer': {
            'Meta': {'object_name': 'DidacticOffer', 'db_table': "'merv_didactic_offer'"},
            'end_date': ('django.db.models.fields.DateField', [], {'db_column': "'end_date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'db_column': "'start_date'"})
        },
        'merovingian.module': {
            'Meta': {'ordering': "['name']", 'object_name': 'Module', 'db_table': "'merv_module'"},
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.ModuleType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'merovingian.moduletype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'ModuleType', 'db_table': "'merv_module_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subject': {
            'Meta': {'ordering': "('semester', 'name', '-type', 'assessment')", 'object_name': 'Subject', 'db_table': "'merv_subject'"},
            'assessment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectAssessment']", 'db_column': "'id_assessment'"}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'hours': ('django.db.models.fields.FloatField', [], {'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']", 'db_column': "'id_module'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'semester': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semester'"}),
            'teachers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trainman.Teacher']", 'null': 'True', 'through': "orm['merovingian.SubjectToTeacher']", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectType']", 'db_column': "'id_type'"})
        },
        'merovingian.subjectassessment': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectAssessment', 'db_table': "'merv_subject_assessment'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subjecttoteacher': {
            'Meta': {'object_name': 'SubjectToTeacher', 'db_table': "'merv_subject__to__teacher'"},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'db_column': "'description'", 'blank': 'True'}),
            'groups': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'groups'"}),
            'hours': ('django.db.models.fields.FloatField', [], {'max_length': '3', 'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']", 'db_column': "'id_subject'"}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']", 'db_column': "'id_teacher'"})
        },
        'merovingian.subjecttype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectType', 'db_table': "'merv_subject_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'metacortex.assessmentform': {
            'Meta': {'ordering': "['order', 'name']", 'object_name': 'AssessmentForm', 'db_table': "'metacortex_assessment_form'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'metacortex.didacticmethodmajor': {
            'Meta': {'ordering': "['order', 'name']", 'object_name': 'DidacticMethodMajor', 'db_table': "'metacortex_didactic_method_major'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'metacortex.didacticmethodminor': {
            'Meta': {'ordering': "['order', 'name']", 'object_name': 'DidacticMethodMinor', 'db_table': "'metacortex_didactic_method_minor'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'major_didactic_method': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.DidacticMethodMajor']", 'db_column': "'id_didactic_method_major'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'metacortex.ects': {
            'Meta': {'ordering': "('is_default', 'name')", 'object_name': 'ECTS'},
            'flag': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'metacortex.lecturelanguage': {
            'Meta': {'object_name': 'LectureLanguage', 'db_table': "'metacortex_lecture_language'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        'metacortex.metacortexnews': {
            'Meta': {'ordering': "('-date', '-is_active')", 'object_name': 'MetacortexNews', 'db_table': "'metacortex_news'"},
            'content': ('django.db.models.fields.TextField', [], {}),
            'content_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'content_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'content_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'content_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'db_column': "'date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'metacortex.practicetype': {
            'Meta': {'object_name': 'PracticeType', 'db_table': "'metacortex_practice_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        'metacortex.subjectdifficulty': {
            'Meta': {'ordering': "['order', 'name']", 'object_name': 'SubjectDifficulty', 'db_table': "'metacortex_subject_difficulty'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'metacortex.syllabus': {
            'Meta': {'object_name': 'Syllabus'},
            'ectss': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['metacortex.ECTS']", 'through': "orm['metacortex.SyllabusToECTS']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'metacortex.syllabusmodule': {
            'Meta': {'ordering': "['module__name']", 'object_name': 'SyllabusModule', 'db_table': "'metacortex_syllabus_module'", '_ormbases': ['metacortex.Syllabus']},
            'additional_information': ('django.db.models.fields.TextField', [], {}),
            'additional_information_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'coordinator': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'module_coordinator'", 'null': 'True', 'to': "orm['trainman.Teacher']"}),
            'lecture_languages': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'lecturelanguages'", 'to': "orm['metacortex.LectureLanguage']", 'db_table': "'metacortex_syllabus_module__to__lecture_language'", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'module': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['merovingian.Module']", 'unique': 'True'}),
            'module_description': ('django.db.models.fields.TextField', [], {}),
            'module_description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'module_description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'module_description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'module_description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'syllabus_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metacortex.Syllabus']", 'unique': 'True', 'primary_key': 'True'}),
            'unit_source': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'unit_source'", 'null': 'True', 'to': "orm['trainman.Department']"}),
            'unit_target': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'unit_target'", 'null': 'True', 'to': "orm['trainman.Department']"})
        },
        'metacortex.syllabuspractice': {
            'Meta': {'ordering': "['subject__name']", 'object_name': 'SyllabusPractice', 'db_table': "'metacortex_syllabus_practice'", '_ormbases': ['metacortex.Syllabus']},
            'additional_information': ('django.db.models.fields.TextField', [], {}),
            'additional_information_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects': ('django.db.models.fields.TextField', [], {}),
            'education_effects_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']"}),
            'syllabus_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metacortex.Syllabus']", 'unique': 'True', 'primary_key': 'True'}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.PracticeType']", 'null': 'True', 'blank': 'True'})
        },
        'metacortex.syllabussubject': {
            'Meta': {'ordering': "['subject__name']", 'object_name': 'SyllabusSubject', 'db_table': "'metacortex_syllabus_subject'", '_ormbases': ['metacortex.Syllabus']},
            'additional_information': ('django.db.models.fields.TextField', [], {}),
            'additional_information_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_information_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'additional_name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'additional_name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'additional_name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'additional_name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'assessment_forms': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['metacortex.AssessmentForm']", 'db_table': "'metacortex_syllabus_subject__to__assessment_form'", 'symmetrical': 'False'}),
            'didactic_methods': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['metacortex.DidacticMethodMinor']", 'db_table': "'metacortex_syllabus_subject__to__didactic_method'", 'symmetrical': 'False'}),
            'education_effects': ('django.db.models.fields.TextField', [], {}),
            'education_effects_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effects_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'initial_requirements': ('django.db.models.fields.TextField', [], {}),
            'initial_requirements_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'initial_requirements_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'initial_requirements_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'initial_requirements_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'literature': ('django.db.models.fields.TextField', [], {}),
            'literature_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'literature_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'literature_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'literature_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']"}),
            'subject_difficulty': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.SubjectDifficulty']", 'null': 'True', 'blank': 'True'}),
            'subjects_scope': ('django.db.models.fields.TextField', [], {}),
            'subjects_scope_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subjects_scope_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subjects_scope_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'subjects_scope_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'syllabus_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metacortex.Syllabus']", 'unique': 'True', 'primary_key': 'True'}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']"})
        },
        'metacortex.syllabustoects': {
            'Meta': {'ordering': "('-ects__is_default', 'ects__order', 'ects__name')", 'object_name': 'SyllabusToECTS', 'db_table': "'metacortex_syllabus__to__ects'"},
            'ects': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.ECTS']", 'db_column': "'id_ects'"}),
            'hours': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'syllabus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metacortex.Syllabus']", 'db_column': "'id_syllabus'"})
        },
        'metacortex.syllabusyear': {
            'Meta': {'ordering': "['-date']", 'object_name': 'SyllabusYear', 'db_table': "'meatcortex_syllabus_year'"},
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'read_only': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'trainman.department': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Department'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_department'", 'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.DepartmentType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'trainman.departmenttype': {
            'Meta': {'object_name': 'DepartmentType', 'db_table': "'trainman_department_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.teacher': {
            'Meta': {'ordering': "('user_profile__user__last_name',)", 'object_name': 'Teacher'},
            'degree': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherDegree']", 'db_column': "'id_degree'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherPosition']", 'db_column': "'id_position'"}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'user_profile'"})
        },
        'trainman.teacherdegree': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherDegree', 'db_table': "'trainman_teacher_degree'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.teacherposition': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherPosition', 'db_table': "'trainman_teacher_position'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.userprofile': {
            'Meta': {'ordering': "('user__last_name',)", 'object_name': 'UserProfile', 'db_table': "'trainman_user_profile'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pesel': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True', 'db_column': "'pesel'", 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'db_column': "'second_name'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'db_column': "'user'"})
        }
    }

    complete_apps = ['metacortex']