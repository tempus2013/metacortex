# -*- coding: utf-8 -*-

from django import forms
from django.utils.encoding import force_unicode
from itertools import chain
from django.forms.widgets import CheckboxInput
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

from apps.metacortex.models import SubjectDifficulty, LectureLanguage, PracticeType
from apps.trainman.models import Department

from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __

from apps.metacortex.settings import SYLLABUS_TYPE_MODULE_ID, SYLLABUS_TYPE_SUBJECT_ID, SYLLABUS_TYPE_PRACTICE_ID, SYLLABUS_TYPE_GENERAL_ID

# --------------------------------------------------------
# --- WIDGETS
# --------------------------------------------------------


class TextEditor(forms.Textarea):
    def __init__(self, *args, **kwargs):
        super(TextEditor, self).__init__(*args, **kwargs)
        self.attrs = {'class': 'ckeditor'}

# -------------------------------------------------------
# --- FORMS
# -------------------------------------------------------

help_text_public = _(u'Check this field if you want the syllabus to be available in a search engine')


class SyllabusModuleForm(forms.Form):
    is_published = forms.BooleanField(required=False, label=_(u"Public"), help_text=help_text_public)
    module_description = forms.CharField(widget=TextEditor, required=False, label=_(u"Module description"))
    additional_information = forms.CharField(widget=TextEditor, required=False, label=_(u"Additional remarks"))
    lecture_languages = forms.MultipleChoiceField(choices=[(ll.id, ll.name) for ll in LectureLanguage.objects.all()], widget=forms.widgets.CheckboxSelectMultiple, required=False, label=_(u"Language of instruction"))
    unit_source = forms.ModelChoiceField(queryset=Department.objects.all(), required=False, label=_(u"Source unit (the one that the module is offered by)"))
    unit_target = forms.ModelChoiceField(queryset=Department.objects.all(), required=False, label=_(u"Target unit (the one that the module is offered for)"))


class SyllabusSubjectForm(forms.Form):
    def __init__(self, *args, **kwargs):
        module_learning_outcomes = kwargs.pop('module_learning_outcomes', [])
        widget = LearningOutcomesCheckboxSelectMultipleWidget(choices=module_learning_outcomes)
        super(SyllabusSubjectForm, self).__init__(*args, **kwargs)
        self.fields['module_learning_outcomes'] = forms.MultipleChoiceField(choices=module_learning_outcomes, widget=widget, required=False, label=_(u"Module learning outcomes realized by subject"))

    is_published = forms.BooleanField(required=False, label=_(u"Public"), help_text=help_text_public)
    additional_name = forms.CharField(widget=forms.TextInput, required=False, label=_(u"Title of course or additional information regarding the course name"))
    additional_information = forms.CharField(widget=TextEditor, required=False, label=_(u"Additional remarks"))
    subject_difficulty = forms.ModelChoiceField(queryset=SubjectDifficulty.objects.all(), required=False, label=_(u"Class level"))
    initial_requirements = forms.CharField(widget=TextEditor, required=False, label=_(u"Prerequisities"))
    subjects_scope = forms.CharField(widget=TextEditor, required=False, label=_(u"List of topics"))
    assessment_conditions = forms.CharField(widget=TextEditor, required=False, label=_(u"Assessment conditions"))
    learning_outcomes_verification = forms.CharField(widget=TextEditor, required=False, label=_(u"Sposób weryfikacji efektów kształcenia"))
    literature = forms.CharField(widget=TextEditor, required=False, label=_(u"Reading list"))
    education_effects = forms.CharField(widget=TextEditor, required=False, label=_(u"Additional learning outcomes"))


class SyllabusPracticeForm(forms.Form):
    is_published = forms.BooleanField(required=False, label=_(u"Public"), help_text=help_text_public)
    description = forms.CharField(widget=TextEditor, required=False, label=_(u"Practice description"))
    additional_information = forms.CharField(widget=TextEditor, required=False, label=_(u"Additional remarks"))
    education_effects = forms.CharField(widget=TextEditor, required=False, label=_(u"Additional learning outcomes"))
    type = forms.ModelChoiceField(queryset=PracticeType.objects.all(), required=False, label=_(u"Type of practice"))


class SyllabusClassicSearchForm(forms.Form):
    """
    Klasyczny formularz wyszukiwania w wyszukiwarce sylabusów.
    """
       
    q = forms.CharField(widget=forms.TextInput, label=_(u"Search"), required=False)
    syllabus_type = forms.ChoiceField(widget=forms.Select, choices=((SYLLABUS_TYPE_MODULE_ID, _(u'Module syllabus')), (SYLLABUS_TYPE_SUBJECT_ID, _(u'Subject syllabus')), (SYLLABUS_TYPE_PRACTICE_ID, _(u'Practice syllabus')), (SYLLABUS_TYPE_GENERAL_ID, _(u'General classes syllabus'))), initial=SYLLABUS_TYPE_SUBJECT_ID, required=False)


class SearchForm(forms.Form):
    """
    Formularz wyszukiwania w zarządzaniu sylabusami.
    """
    name = forms.CharField(label='', required=False)

#---------------------------------------------------
#--- WIDGETY
#---------------------------------------------------

class LearningOutcomesCheckboxSelectMultipleWidget(forms.widgets.CheckboxSelectMultiple):
    """
    Widget z listą wielkorotnego wyboru dla efektów kształcenia. Oprócz pola wyboru wyświetla w wierszu symbol i opis.
    """
    def __init__(self, *args, **kwargs):
        self.choices = kwargs.pop('choices', None)
        super(LearningOutcomesCheckboxSelectMultipleWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = []
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)
        rows = []

        # Normalize to strings
        str_values = set([force_unicode(v) for v in value])
        for i, (id, learning_outcome) in enumerate(chain(self.choices, choices)):

            # If an ID attribute was given, add a numeric index as a suffix,
            # so that the checkboxes don't all have the same ID attribute.
            if has_id:
                final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
                label_for = u' for="%s"' % final_attrs['id']
            else:
                label_for = ''

            # Przygotowanie elementów wiersza
            cb = CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
            id = force_unicode(learning_outcome.id)
            rendered_cb = cb.render(name, id)
            symbol = conditional_escape(force_unicode(learning_outcome.symbol))
            description = conditional_escape(force_unicode(learning_outcome.description))

            # Wygenerowanie wiersza
            row = '<tr>'
            row += '<td class="input">%s</td>' % rendered_cb
            row += '<td class="symbol"><label %s>%s</label></td>' % (label_for, symbol)
            row += '<td><label %s>%s</label></td>' % (label_for, description)
            row += '</tr>'

            # Dodanie wiersza do listy wierszy
            rows.append(row)

        # Wygenerowanie tabeli
        output = u''
        if rows:
            output += '<table class="table_0">'
            output += '<tbody>'
            output += '%s' % u'\n'.join(rows)
            output += '</tbody>'
            output += '</table>'
        else:
            output += '<ul class="infolist"><li>%s</li></ul>' % __(u'List of learning outcomes is empty.')
        return mark_safe(output)