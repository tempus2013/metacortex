# -*- coding: utf-8 -*-

from django.db import models
from apps.merovingian.models import Module, Subject
from apps.trainman.models import Teacher, Department
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

from datetime import date

from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __

from apps.trinity.models import ModuleLearningOutcome
from apps.metacortex.templatetags.syllabus import to_roman

# ---------------------------------------------------
# --- SYLLABUS TYPES
# ---------------------------------------------------

SYLLABUS_MODULE = 0
SYLLABUS_SUBJECT = 1
SYLLABUS_PRACTICE = 2

# ---------------------------------------------------
# --- MANAGERS
# ---------------------------------------------------


class PublishedManager(models.Manager):
    def get_query_set(self):
        return super(PublishedManager, self).get_query_set().filter(is_published=True, is_active=True)

# ---------------------------------------------------
# --- NEWS
# ---------------------------------------------------


class MetacortexNews(models.Model):
    class Meta:
        db_table = 'metacortex_news'
        ordering = ('-date', '-is_active')
        verbose_name = _(u'News')
        verbose_name_plural = _(u'News')
        
    content = models.TextField(verbose_name=_(u'Content'))
    date = models.DateField(db_column='date', verbose_name=_(u'Date'))
    is_active = models.BooleanField()

    def __unicode__(self):
        return self.content[:100]

# ---------------------------------------------------
# --- SYLLABUS
# ---------------------------------------------------


class Syllabus(models.Model):
    class Meta:
        db_table = 'metacortex_syllabus'
        verbose_name = _(u'Syllabus')
        verbose_name_plural = _(u'Syllabuses')

    is_published = models.BooleanField(verbose_name=_(u'Published'))
    is_active = models.BooleanField(default=True, verbose_name=_(u'Active'))
    code = models.CharField(max_length=128)
    ectss = models.ManyToManyField('ECTS', through='SyllabusToECTS')
    
    def get_equivalents(self):
        """
        Zwraca queryset wszystkich ekwiwalnetów godzinowych punktów ECTS przypisanych do sylabusa.
        """
        return self.syllabustoects_set.all()
    
    def get_show_url(self):
        raise NotImplementedError

# ---------------------------------------------------
# --- ECTS
# ---------------------------------------------------


class SyllabusToECTS(models.Model):
    class Meta:
        db_table = 'metacortex_syllabus__to__ects'
        ordering = ('-ects__is_default', 'ects__order', 'ects__name',)
        
    syllabus = models.ForeignKey('Syllabus', db_column='id_syllabus')
    ects = models.ForeignKey('ECTS', db_column='id_ects')
    hours = models.FloatField(null=True, blank=True)
    
    def get_delete_url(self):
        return reverse('apps.metacortex.views.syllabus_my.syllabus_delete_ects', kwargs={'id_syllabus': self.syllabus.id, 'id_ects': self.ects.id})


class ECTS(models.Model):
    class Meta:
        db_table = 'metacortex_ects'
        verbose_name = _(u'ECTS equivalent in hours')
        verbose_name_plural = _(u'ECTS equivalents in hours')
        ordering = ('is_default', 'name')
        
    name = models.CharField(max_length=255)
    is_default = models.BooleanField()
    order = models.IntegerField(null=True, blank=True)
    flag = models.IntegerField(null=True, blank=True)
    
    def __unicode__(self):
        return unicode(self.name[:100]+"...")

# ---------------------------------------------------
# --- SYLLABUS MODULE
# ---------------------------------------------------


class SyllabusModule(Syllabus):
    class Meta:
        db_table = 'metacortex_syllabus_module'
        verbose_name = _(u'Module syllabus')
        verbose_name_plural = _(u'Module syllabuses')
        ordering = ['module__name']
        
    module = models.OneToOneField(Module)
    module_description = models.TextField()
    lecture_languages = models.ManyToManyField('LectureLanguage', null=True, blank=True, db_table='metacortex_syllabus_module__to__lecture_language', verbose_name=_(u'Languages'), related_name="lecturelanguages")
    unit_source = models.ForeignKey(Department, null=True, blank=True, related_name='unit_source')
    unit_target = models.ForeignKey(Department, null=True, blank=True, related_name='unit_target')
    coordinator = models.ForeignKey(Teacher, null=True, blank=True, related_name="module_coordinator", verbose_name=_(u'Coordinator'))
    additional_information = models.TextField()

    objects = models.Manager()
    objects_published = PublishedManager()

    def get_show_url(self):
        return reverse('apps.metacortex.views.search_engine.syllabus_show', kwargs={'syllabus_type': 0, 'syllabus_id': self.pk})
    
    def __unicode__(self):
        return self.module.name
    
    def display_as_list(self):
        """
        Wyświetla informacje o sylabusie w postaci listy.
        Używane do wyświetlania wyników wyszukiwania.
        """
        
        coordinator = unicode(self.coordinator).encode('utf-8') if self.coordinator else '&ndash;'
        department = unicode(self.module.get_department().name).encode('utf-8') if self.module.get_department() else '&ndash;'
        
        result = '<h2><a class="link_0" target="_blank" href="%s">%s</a></h2>' % (reverse('apps.metacortex.views.search_engine.syllabus_show', kwargs={'syllabus_type': 0, 'syllabus_id': self.pk}), unicode(self.module.name).encode('utf-8'))
        result += '<ul class="square result_description">'
        result += '<li>%s: %s</li>' % (unicode(__('Organizational unit')).encode('utf-8'), department)
        result += '<li>%s: %s</li>' % (unicode(__('Course')).encode('utf-8'), unicode(self.module.get_course()).encode('utf-8'))
        if self.code:
            result += '<li>%s: %s</li>' % (u'Kod modułu'.encode('utf-8'), unicode(self.code).encode('utf-8'))
        result += '<li>%s: %s</li>' % (unicode(__('Coordinator')).encode('utf-8'), coordinator)
        result += '</ul>'
        return mark_safe(result)

# ---------------------------------------------------
# --- SYLLABUS SUBJECT
# ---------------------------------------------------


class AbstractSyllabusSubject(models.Model):
    class Meta:
        abstract = True
        
    subject = models.ForeignKey(Subject)
    learning_outcomes_verification = models.TextField()
    
    def get_hours(self):
        return self.subject.hours
        
    def get_ects(self):
        """
        Zwraca liczbę punktów ECTS sylabusa.
        """
        return self.subject.ects

    def display_ects_section(self):
        """
        Zwraca wartość True jeżeli dla sylabusa powinna zostać wyświetlona sekcja ECTS.
        Sekcja ECTS dla sylabusa zostanie wyświetlona jeżeli przedmiot sylabusa ma okresloną liczbę puntków ECTS.
        """
        if self.subject.ects:
            return True
        return False


class SyllabusSubject(Syllabus, AbstractSyllabusSubject):
    class Meta:
        db_table = 'metacortex_syllabus_subject'
        verbose_name = _(u'Subject syllabus')
        verbose_name_plural = _(u'Subject syllabuses')
        ordering = ['subject__name']
    
    subject_difficulty = models.ForeignKey('SubjectDifficulty', null=True, blank=True)
    additional_name = models.CharField(max_length=255)
    initial_requirements = models.TextField()
    literature = models.TextField()
    subjects_scope = models.TextField()
    assessment_conditions = models.TextField(null=True, blank=True)
    additional_information = models.TextField()
    education_effects = models.TextField()
    module_learning_outcomes = models.ManyToManyField(ModuleLearningOutcome, db_table='metacortex_syllabus_subject__to__module_learning_outcome', verbose_name=_(u'Module learning outcomes'))
    didactic_methods = models.ManyToManyField('DidacticMethodMinor', db_table='metacortex_syllabus_subject__to__didactic_method', verbose_name=_(u'Teaching methods'))
    assessment_forms = models.ManyToManyField('AssessmentForm', db_table='metacortex_syllabus_subject__to__assessment_form', verbose_name=_(u'Evaluation form'))
    teacher = models.ForeignKey(Teacher)

    def get_show_url(self):
        return reverse('apps.metacortex.views.search_engine.syllabus_show', kwargs={'syllabus_type': 1, 'syllabus_id': self.pk})
    
    def get_name(self):
        name = self.subject.name
        if self.additional_name:
            name += ' (%s)' % self.additional_name
        return name                    
    
    def __unicode__(self):
        syllabus_name = self.subject.name
        if self.additional_name:
            syllabus_name += " (%s)" % self.additional_name
        syllabus_name += ", %s %s" % (self.subject.hours, __(u"h"))
        syllabus_name += ", %s" % self.subject.type.name
        syllabus_name += " (%s)" % unicode(self.teacher)
        return syllabus_name
    
    def display_as_list(self):
        """
        Wyświetla informacje o sylabusie w postaci listy.
        Używane do wyświetlania wyników wyszukiwania.
        """
        department = self.subject.module.get_department()
        department = unicode(department.name).encode('utf-8') if department else '&ndash;'
        
        if self.subject.is_annual():
            year_semester = "%s/-" % self.subject.semester
        else:
            year_semester = "%s/%s" % (to_roman(self.subject.get_year_semester()[0]), self.subject.semester)
        
        result = '<h2><a class="link_0" target="_blank" href="%s">%s</a></h2>' % (reverse('apps.metacortex.views.search_engine.syllabus_show', kwargs={'syllabus_type': 1, 'syllabus_id': self.pk}), unicode(self.get_name()).encode('utf-8'))
        result += '<ul class="square result_description">'
        result += '<li>%s: %s</li>' % (unicode(__('Teacher')).encode('utf-8'), unicode(self.teacher).encode('utf-8'))
        result += '<li>%s: %s</li>' % (unicode(__('Organizational unit')).encode('utf-8'), department)
        result += '<li>%s: %s</li>' % (unicode(__('Course')).encode('utf-8'), unicode(self.subject.module.get_course()).encode('utf-8'))
        result += '<li>%s: %s</li>' % (unicode(__('Speciality')).encode('utf-8'), unicode(self.subject.module.get_sgroup().name).encode('utf-8'))
        result += '<li>%s: %s</li>' % (unicode(__('Module')).encode('utf-8'), unicode(self.subject.module.name).encode('utf-8'))
        result += '<li>%s: %s (%s %s)</li>' % (unicode(_('Type of the class')).encode('utf-8'), unicode(self.subject.type).encode('utf-8'), unicode(self.subject.hours).encode('utf-8'), unicode(__(u'h')).encode('utf-8'))
        if self.code:
            result += '<li>%s: %s</li>' % (unicode(__('Kod przedmiotu')).encode('utf-8'), unicode(self.code).encode('utf-8'))
        result += '<li>%s: %s</li>' % (unicode(__('Year/Semester')).encode('utf-8'), year_semester)
        result += '</ul>'
        return mark_safe(result)
    
    objects = models.Manager()
    objects_published = PublishedManager()

# ---------------------------------------------------
# --- SYLLABUS PRACTICE
# ---------------------------------------------------    


class SyllabusPractice(Syllabus, AbstractSyllabusSubject):
    class Meta:
        db_table = 'metacortex_syllabus_practice'
        verbose_name = _(u'Practice syllabus')
        verbose_name_plural = _(u'Practice syllabuses')
        ordering = ['subject__name']
        
    teacher = models.ForeignKey(Teacher)
    type = models.ForeignKey('PracticeType', null=True, blank=True)
    description = models.TextField()
    education_effects = models.TextField()
    additional_information = models.TextField()

    def get_show_url(self):
        return reverse('apps.metacortex.views.search_engine.syllabus_show', kwargs={'syllabus_type': 2, 'id_syllabus': self.pk})
    
    def __unicode__(self):
        syllabus_name = self.subject.name
        syllabus_name += ", %s %s" % (self.subject.hours, __(u"h"))
        syllabus_name += ", %s" % self.subject.type.name
        syllabus_name += " (%s)" % unicode(self.teacher)
        return syllabus_name
    
    def display_as_list(self):
        """
        Wyświetla informacje o sylabusie w postaci listy.
        Używane do wyświetlania wyników wyszukiwania.
        """
        
        department = self.subject.module.get_department()
        department = unicode(department.name).encode('utf-8') if department else '&ndash;'
        
        if self.subject.is_annual():
            year_semester = "%s/-" % self.subject.semester
        else:
            year_semester = "%s/%s" % (to_roman(self.subject.get_year_semester()[0]), self.subject.semester)
        
        result = '<h2><a class="link_0" target="_blank" href="%s">%s</a></h2>' % (reverse('apps.metacortex.views.search_engine.syllabus_show', kwargs={'syllabus_type': 2, 'syllabus_id': self.pk}), unicode(self.subject.name).encode('utf-8'))
        result += '<ul class="square result_description">'
        result += '<li>%s: %s</li>' % (unicode(__('Practice coordinator')).encode('utf-8'), unicode(self.teacher).encode('utf-8'))
        result += '<li>%s: %s</li>' % (unicode(__('Organizational unit')).encode('utf-8'), department)
        result += '<li>%s: %s</li>' % (unicode(__('Course')).encode('utf-8'), unicode(self.subject.module.get_course()).encode('utf-8'))
        result += '<li>%s: %s</li>' % (unicode(__('Speciality')).encode('utf-8'), unicode(self.subject.module.get_sgroup().name).encode('utf-8'))
        result += '<li>%s: %s</li>' % (unicode(__('Module')).encode('utf-8'), unicode(self.subject.module.name).encode('utf-8'))
        result += '<li>%s: %s</li>' % (unicode(__('Year/Semester')).encode('utf-8'), year_semester)
        result += '</ul>'
        return mark_safe(result)
    
    objects = models.Manager()
    objects_published = PublishedManager()

# ---------------------------------------------------
# --- PRACTICE TYPE
# ---------------------------------------------------  


class PracticeType(models.Model):
    class Meta:
        db_table = 'metacortex_practice_type'
        verbose_name = _(u'Type of practice')
        verbose_name_plural = _(u'Types of practices')
    
    name = models.CharField(max_length=64)
    
    def __unicode__(self):
        return unicode(self.name)
    
# ---------------------------------------------------
# --- LECTURE LANGUAGE
# ---------------------------------------------------      


class LectureLanguage(models.Model):
    class Meta:
        db_table = 'metacortex_lecture_language'
        verbose_name = _(u'Language')
        verbose_name_plural = _(u'Languages')
      
    name = models.CharField(max_length=64)
    
    def __unicode__(self):
        return unicode(self.name)
    
   
# ---------------------------------------------------
# --- DIDACTIC METHOD MAJOR
# ---------------------------------------------------     


class DidacticMethodMajor(models.Model):
    class Meta:
        db_table = 'metacortex_didactic_method_major'
        verbose_name = _(u'Major teaching method')
        verbose_name_plural = _(u'Major teaching methods')
        ordering = ['order', 'name']
    
    name = models.CharField(max_length=128)
    order = models.IntegerField(default=0)
    
    def __unicode__(self):
        return unicode(self.name)

# ---------------------------------------------------
# --- DIDACTIC METHOD MINOR
# ---------------------------------------------------  


class DidacticMethodMinor(models.Model):
    class Meta:
        db_table = 'metacortex_didactic_method_minor'
        verbose_name = _(u'Minor teachong method')
        verbose_name_plural = _(u'Minior teachong methods')
        ordering = ['order', 'name']
    
    name = models.CharField(max_length=128)
    order = models.IntegerField(default=0)
    major_didactic_method = models.ForeignKey('DidacticMethodMajor', db_column='id_didactic_method_major', verbose_name=_(u'Major teaching method'))
    
    def __unicode__(self):
        return unicode(self.name)

# ---------------------------------------------------
# --- ASSESSMENT FORM
# ---------------------------------------------------  


class AssessmentForm(models.Model):
    class Meta:
        db_table = 'metacortex_assessment_form'
        verbose_name = _(u'Evaluation form')
        verbose_name_plural = _(u'Evaluation forms')
        ordering = ['order', 'name']
    
    name = models.CharField(max_length=128)
    order = models.IntegerField(default=0)
    
    def __unicode__(self):
        return unicode(self.name)
    
# ---------------------------------------------------
# --- SUBJECT DIFFICULTY
# ---------------------------------------------------      


class SubjectDifficulty(models.Model):
    class Meta:
        db_table = 'metacortex_subject_difficulty'
        verbose_name = _(u'Class level')
        verbose_name_plural = _(u'Class levels')
        ordering = ['order', 'name']
    
    name = models.CharField(max_length=128)
    order = models.IntegerField(default=0)
    
    def __unicode__(self):
        return unicode(self.name)
    
# ---------------------------------------------------
# --- YEARS
# ---------------------------------------------------


class SyllabusYear(models.Model):
    class Meta:
        db_table = 'meatcortex_syllabus_year'
        verbose_name = _(u'Syllabus year')
        verbose_name_plural = _(u'Syllabuses years')
        ordering = ['-date', ]
        
    date = models.DateField()
    read_only = models.BooleanField()
    
    def get_previous(self):
        previous_year = self.date.year-1
        try:
            return SyllabusYear.objects.get(date__year=previous_year)
        except:
            return None
    
    def __unicode__(self):
        return unicode(self.date.year)

    def get_name_with_current_year(self):
        x = date.today().year - self.date.year + 1
        return u"{0} (obecnie rok studiów: {1})".format(unicode(self.date.year), x)


# ---------------------------------------------------
# --- ADMINISTRATOR
# ---------------------------------------------------

from apps.trainman.models import UserProfile, Department

class MetacortexProfile(models.Model):
    class Meta:
        verbose_name = _(u'Administrator')
        verbose_name_plural = _(u'Administrators')

    user_profile = models.OneToOneField(UserProfile, verbose_name=_(u'User'))
    departments = models.ManyToManyField(Department, null=True, blank=True)