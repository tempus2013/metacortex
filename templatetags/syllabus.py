# -*- coding: utf-8 -*-

from collections import OrderedDict, defaultdict
from apps.merovingian.models import SubjectType

from django import template
register = template.Library()

@register.inclusion_tag('metacortex/templatetags/syllabus_module_info.html')
def module_info(syllbaus_module):
    """
    Wyświetla sylabus modułu.
    """
    return {'syllabus': syllbaus_module}

@register.inclusion_tag('metacortex/templatetags/syllabus_subject_info.html')
def subject_info(syllabus_subject):
    """
    Wyświetla sylabus przedmiotu.
    """
    return {'syllabus': syllabus_subject}

@register.inclusion_tag('metacortex/templatetags/syllabus_practice_info.html')
def practice_info(syllabus_practice):
    """
    Wyświetla sylabus praktyk.
    """
    return {'syllabus': syllabus_practice}

@register.inclusion_tag('metacortex/templatetags/syllabusses_list.html')
def syllabusses_list(syllabusses):
    """
    Wyświetla listę sylabusów przekazanych jako parametr.
    """
    return {'syllabusses': syllabusses}

@register.inclusion_tag('metacortex/templatetags/ects.html')
def ects(selected_year, syllabus):
    """
    Wyświetla sekecję pozwalającą na definiowanie punktów ECTS dla sylabusa. Sekcja ECTS nie powinna być wyświetlana dla sylabusa, który nie ma przyporządkowanej liczby punktów ECTS.
    """
    from apps.metacortex.models import SyllabusPractice, SYLLABUS_SUBJECT, SYLLABUS_PRACTICE

    all_hours = syllabus.get_ects()*30  # Jezeli kiedys ta wartość się zmieni (30) wtedy trzeba będzie ją przenieść do bazy danych i definiować ją oddzielnie dla każdego semestru.
    
    remain_hours = all_hours
    for equivalent in syllabus.get_equivalents():
        if equivalent.hours:
            remain_hours -= equivalent.hours
    
    show_remain_hours_warning = False        
    if remain_hours < 0: 
        show_remain_hours_warning = True

    syllabus_type = SYLLABUS_SUBJECT
    if isinstance(syllabus, SyllabusPractice):
        syllabus_type = SYLLABUS_PRACTICE

    return {
        'selected_year': selected_year,
        'syllabus_type': syllabus_type,
        'syllabus': syllabus,
        'remain_hours': remain_hours,
        'show_remain_hours_warning': show_remain_hours_warning
    }

@register.filter
def mul(a, b):
    return float(a)*float(b)

@register.filter
def to_roman(a):
    """
    :param a: Arabic number.
    :return: Roman number.
    """
    a = int(a)
    romans = ['I', 'II', 'III', 'IV', 'V', 'VII', 'VIII', 'IX', 'X']
    return romans[a-1] if 0 < a <= len(romans) else a


@register.inclusion_tag('metacortex/templatetags/module_details.html')
def module_details_table(module):
    details = OrderedDict()
    semesters = range(1, module.get_course().semesters + 1)
    has_module_properties = True if module.moduleproperties_set.all().count() else False
    for semester in semesters:
        details[semester] = []

        if has_module_properties:
            subjects = module.moduleproperties_set.filter(semester=semester)
        else:
            subjects = module.subject_set.filter(semester=semester)

        if subjects.count():
            subject_type_ids = subjects.order_by('type').values_list('type', flat=True).distinct()
            for subject_type_id in subject_type_ids:
                subject_type = SubjectType.objects.get(id=subject_type_id)

                subject_group = {'type': subject_type, 'hours': 0, 'ects': 0}
                subjects_by_type = subjects.filter(type=subject_type)
                for subject_by_type in subjects_by_type:
                    subject_group['hours'] += subject_by_type.hours
                    subject_group['ects'] += subject_by_type.ects if subject_by_type.ects else 0

                details[semester].append(subject_group)

        total_hours = 0
        total_ects = 0
        for subject_group in details.values():
            for subject in subject_group:
                total_hours += subject['hours']
                total_ects += subject['ects']

    return {
        'details': details,
        'total_hours': total_hours,
        'total_ects': total_ects
    }

